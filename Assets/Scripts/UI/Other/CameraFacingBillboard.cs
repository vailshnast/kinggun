﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraFacingBillboard : MonoBehaviour
{
    public Transform enemy;

    private Camera m_Camera;

    private Vector3 offset;

    public Image img1;
    public Image img2;

    public bool isActivated;

    void Start()
    {
        m_Camera = Camera.main;
        transform.parent.GetComponent<Canvas>().worldCamera = m_Camera;
        offset = transform.position - enemy.transform.position;
    }

    void FixedUpdate()
    {
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
            m_Camera.transform.rotation * Vector3.up);

        transform.position = enemy.transform.position + offset;

    }

    public void Activate()
    {
        isActivated = true;
        img1.enabled = true;
        img2.enabled = true;
    }
}