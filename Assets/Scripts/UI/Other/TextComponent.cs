﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;


public class TextComponent : MonoBehaviour
{


    private Camera m_Camera;
    private TextMeshPro textMeshPro;
    void Awake()
    {
        textMeshPro = GetComponent<TextMeshPro>();
        m_Camera = Camera.main;
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward,
            m_Camera.transform.rotation * Vector3.up);
    }

    public void StartAnimation(string content, float fontSize, Color color, Vector3 worldPoint)
    {
        transform.position = worldPoint;
        textMeshPro.text = content;
        textMeshPro.fontSize = fontSize;
        textMeshPro.color = color;
        StartCoroutine(MoveText(0.5f, 0.25f, 3));
    }
    public void StartAnimation(string content, float fontSize, Color color, Vector3 worldPoint,float live_time, float fade_Time,float speed)
    {
        transform.position = worldPoint;
        textMeshPro.text = content;
        textMeshPro.fontSize = fontSize;
        textMeshPro.color = color;
        StartCoroutine(MoveText(live_time, fade_Time, speed));
    }

    private IEnumerator MoveText(float live_time, float fade_time, float speed)
    {
        float t = 0f;

        while (t <= 1)
        {
            t += Time.deltaTime / live_time;
            transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime * speed, transform.position.z);
            yield return null;
        }

      

        Color startColor = textMeshPro.color;
        Color targetColor = new Color(startColor.r, startColor.g, startColor.b, 0);

         t = 0f;

        while (t <= 1)
        {
            t += Time.deltaTime / fade_time;
            textMeshPro.color = Color.Lerp(startColor, targetColor, t);
            transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime * speed, transform.position.z);
            yield return null;
        }
        textMeshPro.color = targetColor;
        Destroy(gameObject);
    }
}
