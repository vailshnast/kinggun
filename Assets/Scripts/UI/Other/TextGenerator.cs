﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextGenerator : MonoBehaviour
{

    public static TextGenerator instance;

    public GameObject textPrefab;

    private Camera cam;

    void Awake()
    {
        instance = this;
        cam = Camera.main;
    }

    public void GenerateText(string content, float fontSize, Color color, Vector3 worldPoint)
    {
        GameObject textObject = Instantiate(textPrefab, worldPoint, Quaternion.identity, transform);
        textObject.GetComponent<TextComponent>().StartAnimation(content, fontSize, color, worldPoint);
    }

 

}
