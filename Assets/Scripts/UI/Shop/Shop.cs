﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Shop : MonoBehaviour
{

    public static Shop instance;
    private List<ShopSlot> shopSlotsList;

    private Dictionary<ItemType, List<ItemInfo>> shopItems;

    public List<ItemInfoTemplate> itemTemplates;

    public float rerandomDelay = 0.5f;

    public float currentDelayTime;

    private ItemType currentOpenedTab;

    public GameObject shopActivationTooltip;
    public GameObject shopModel;

    public bool canBeOpened;

    private GameObject panel;

    public bool isOpened
    {
        get { return panel.activeInHierarchy; }
    }

    void Awake()
    {
        instance = this;

        shopItems = new Dictionary<ItemType, List<ItemInfo>>
        {
            {ItemType.Weapon, new List<ItemInfo>()},
            {ItemType.EnergyBlock, new List<ItemInfo>()},
            {ItemType.BodyArmour, new List<ItemInfo>()}
        };

        panel = transform.GetChild(0).gameObject;

        InitializeSlots();

        currentDelayTime = rerandomDelay * 60;
        StartCoroutine(RerandomWithDelay());

    }

    void Start()
    {   
        RandomShopItems();    
        ShowShopItems(ItemType.Weapon);
    }

    void Update()
    {
        if(shopActivationTooltip.activeInHierarchy)
            shopActivationTooltip.transform.position = new Vector3(Camera.main.WorldToScreenPoint(shopModel.transform.position).x, Camera.main.WorldToScreenPoint(shopModel.transform.position).y + 100, 0);
    }

    private void InitializeSlots()
    {
        shopSlotsList = GetComponentsInChildren<ShopSlot>(true).ToList();

        for (int i = 0; i < shopSlotsList.Count; i++)
        {
            shopSlotsList[i].InitializeSlot();
        }
    }

    private IEnumerator RerandomWithDelay()
    {
        while (currentDelayTime > 0)
        {
            currentDelayTime -= Time.deltaTime;
            yield return null;
        }
        RandomShopItems();
        ShowShopItems(currentOpenedTab);
        currentDelayTime = rerandomDelay * 60;
        StartCoroutine(RerandomWithDelay());
    }

    public void OpenShop()
    {
        StopAllCoroutines();
        
        shopActivationTooltip.SetActive(false);

        panel.SetActive(true);
    }

    public void ShowActivationTooltip()
    {
        canBeOpened = true;
        shopActivationTooltip.SetActive(true);
    }
    public void HideActivationTooltip()
    {
        shopActivationTooltip.SetActive(false);
    }

    public void CloseShop()
    {    
        Inventory.instance.HideTooltip();    
        shopActivationTooltip.SetActive(true);
        panel.SetActive(false);
        StartCoroutine(RerandomWithDelay());
    }

    private void RandomShopItems()
    {
        shopItems[ItemType.Weapon].Clear();
        shopItems[ItemType.EnergyBlock].Clear();
        shopItems[ItemType.BodyArmour].Clear();

        for (int i = 0; i < 36; i++)
        {
            AddItemToShop(ItemType.Weapon);
        }

        for (int i = 0; i < 10; i++)
        {
           
            AddItemToShop(ItemType.EnergyBlock);
            AddItemToShop(ItemType.BodyArmour);
        }
    }

    private void AddItemToShop(ItemType type)
    {
        ItemInfoTemplate itemTemplate = itemTemplates.FirstOrDefault(x => x.type == type);

        if (type == ItemType.Weapon)
        {
            List<ItemInfoTemplate> weapons = itemTemplates.Where(t => t.type == ItemType.Weapon).ToList();
            itemTemplate = weapons[Random.Range(0, weapons.Count)];
        }



        if (itemTemplate == null)
        {
            Debug.Log("there are no such items abandon");
            return;
        }

        shopItems[itemTemplate.type].Add(new ItemInfo(itemTemplate, Mathf.Clamp(Random.Range(Player.player.level - 1, Player.player.level + 3), 1, 1000)));
        //shopItems[itemTemplate.type].Add(new ItemInfo(itemTemplate, 10));
    }

    public void RemoveItemFromShop(ItemInfo item)
    {
        shopItems[item.type].Remove(item);
    }

    private void ShowShopItems(ItemType type)
    {
        currentOpenedTab = type;

        for (int i = 0; i < shopSlotsList.Count; i++)
        {
            shopSlotsList[i].RemoveItem();
        }

        for (int i = 0; i < shopItems[type].Count;i++)
        {
            shopSlotsList[i].SetUpItem(shopItems[type][i]);
        }
    }

    public void ToggleSlotTab(ItemType type)
    {
        ShowShopItems(type);
    }
	
}


