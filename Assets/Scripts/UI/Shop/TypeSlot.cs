﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TypeSlot : MonoBehaviour,IPointerDownHandler
{
    public ItemType slotType;

    public void OnPointerDown(PointerEventData eventData)
    {
       Shop.instance.ToggleSlotTab(slotType);
    }
}
