﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopSlot : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler,IPointerDownHandler
{
    [HideInInspector]
    public ItemInfo itemInfo;
    [HideInInspector]
    public bool IsEmpty;

    private Image imageComponent;

    public void InitializeSlot()
    {
        imageComponent = transform.GetChild(0).GetComponent<Image>();
        itemInfo = null;
        IsEmpty = true;
    }

    public void SetUpItem(ItemInfo info)
    {
        IsEmpty = false;

        itemInfo = info;

        imageComponent.sprite = itemInfo.itemSprite;

        imageComponent.enabled = true;
    }

    public void RemoveItem()
    {
        IsEmpty = true;
        itemInfo = null;
        imageComponent.enabled = false;
    }

    public void BuyItem()
    {
        if (itemInfo != null)
        {
            if (Inventory.instance.AddItemToInventory(itemInfo) && itemInfo.GetBuyPrice() <= Inventory.instance.data.credits)
            {
                Shop.instance.RemoveItemFromShop(itemInfo);
                Inventory.instance.AddCredit(-itemInfo.GetBuyPrice());
                IsEmpty = true;
                itemInfo = null;
                imageComponent.enabled = false;
            }
        }

         
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(itemInfo!=null)
        Inventory.instance.ShowTooltip(itemInfo,true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Inventory.instance.HideTooltip();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        BuyItem();
    }
}
