﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMarker : MonoBehaviour
{

    public MapIconTemplate template;

    private MapIcon icon;

	// Use this for initialization
	void Start ()
	{
	  icon =  MiniMap.instance.AddIconToMap(template, transform);
	}

    public void RemoveMarker()
    {
        MiniMap.instance.RemoveIconFromMap(icon);
    }
}
