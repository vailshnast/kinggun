﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour
{
    public GameObject mapIconPrefab;

    private List<MapIcon> mapIcons = new List<MapIcon>();

    public static MiniMap instance;

    private RectTransform canvas;

    private RectTransform miniMap;

    private RectTransform menuMap;

    public List<MapIconTemplate> iconTemplateList;

    public float realWorldRadius = 15;

    public RectTransform mapRect;

    public Transform player;

    public Image playerImage;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMap();
    }

    private void UpdateMap()
    {
        for (int i = mapIcons.Count - 1; i >= 0; i--)
        {
            mapIcons[i].SetIconPos(GetMiniMapPos(mapIcons[i]));
        }
        playerImage.transform.rotation = Quaternion.Euler(0, 0, -player.transform.rotation.eulerAngles.y);
    }
    public MapIcon AddIconToMap(MapIconTemplate iconTemplate, Transform objectInstance)
    {
        GameObject mapIconInstance = Instantiate(mapIconPrefab, transform.Find(iconTemplate.groupType.ToString()), false);

        MapIcon mapIcon = new MapIcon(mapIconInstance, objectInstance, iconTemplate)
        {
            /*rectComponent =
            {
                anchorMin = mapRect.anchorMax,
                anchorMax = mapRect.anchorMax
            }*/
        };


        mapIcons.Add(mapIcon);
        return mapIcon;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5F);
        Gizmos.DrawCube(player.position, new Vector3(realWorldRadius, 1, realWorldRadius));
    }
    public void RemoveIconFromMap(MapIcon icon)
    {
        mapIcons.Remove(icon);
        Destroy(icon.rectComponent.gameObject);
    }
    private Vector2 GetMiniMapPos(MapIcon icon)
    {
        float koef = realWorldRadius / mapRect.sizeDelta.x; /*mapRect.sizeDelta.x / (realWorldRadius / 2);*/
        Vector3 playerPos = Player.player.transform.position;
        Vector3 objectPos = icon.objectInstance.position;
        Vector3 mapPos = (objectPos - playerPos) / koef;

        mapPos = new Vector2(mapPos.x /*+ mapRect.anchoredPosition.x*/, mapPos.z /*+ mapRect.anchoredPosition.y*/);
        //
      
        float rightEdge = mapRect.sizeDelta.x / 2 /*+ mapRect.anchoredPosition.x*/ + icon.HalfedSize().x;
        float leftEdge = -mapRect.sizeDelta.x / 2 /*+ mapRect.anchoredPosition.x*/ - icon.HalfedSize().x ;

        float topEdge = mapRect.sizeDelta.y / 2 /*+ mapRect.anchoredPosition.y*/ + icon.HalfedSize().y;
        float bottomEdge = -mapRect.sizeDelta.y / 2 /*+ mapRect.anchoredPosition.y*/ - icon.HalfedSize().y;

        if (icon.iconType == IconType.Clamp)
        {
            mapPos = new Vector2(Mathf.Clamp(mapPos.x, leftEdge, rightEdge), Mathf.Clamp(mapPos.y, bottomEdge, topEdge));
        }
        else
        {
            if (mapPos.x > rightEdge || mapPos.x < leftEdge || mapPos.y > topEdge || mapPos.y < bottomEdge)
                icon.imageComponent.enabled = false;
            else
                icon.imageComponent.enabled = true;
        }


        return mapPos;
    }
}

public class MapIcon
{ 
    public Image imageComponent { get; private set; }
    public RectTransform rectComponent { get; private set; }

    private Vector2 size;

    public IconType iconType { get; private set; }

    public Transform objectInstance { get; private set; }


    public MapIcon(GameObject iconInstance,Transform objectInstance, MapIconTemplate template)
    {
        this.objectInstance = objectInstance;
        imageComponent = iconInstance.GetComponent<Image>();
        rectComponent = iconInstance.GetComponent<RectTransform>();

        iconType = template.iconType;

        size = template.size;
        imageComponent.sprite = template.sprite;
        rectComponent.sizeDelta = template.size;        
    }

    public Vector2 HalfedSize()
    {
        return size / 2;
    }

    public void SetIconPos(Vector2 pos)
    {
        rectComponent.anchoredPosition = pos;
    }
}


public enum GroupType
{
    Teleport,
    Enemy,
    Box
}

public enum IconType
{
    Clamp,
    Hide
}
