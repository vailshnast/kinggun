﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShowSkillInfo : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
{

    public Image spellImageComponent;


    private SpellType spellType;



    public void SetSkill(SpellType spellType, Sprite image)
    {
        this.spellType = spellType;
        spellImageComponent.sprite = image;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        string description = "";

        #region Decsription

        switch (spellType)
        {
            case SpellType.Heal:

                description = "<align=center>Heal Spell</align>" + "\n" + "Heals " + string.Format("{0:0.0}", 50 + (float) 50 / 100 * PlayerStats.instance.GetStatAmount(PropertyType.SkillPower)) + " points";

                break;
            case SpellType.Grenade:

                description = "<align=center>ITS TIME TO JACK TO LET A RIP</align>" + "\n" + "ITS TIME TO LEAVE THEM ALL BEHIIIIIIIIIIIIIIIIIIIIIIIND";

                break;
            case SpellType.Shield:
                description = "<align=center>Shield Spell</align>\n" + "Creates a shield for " + string.Format("{0:0.0}", 3 + (float) 3 / 100 * PlayerStats.instance.GetStatAmount(PropertyType.SkillPower)) + " seconds\nCooldown:18 sec";

                break;
        }


        #endregion


        Inventory.instance.ShowTooltip(description);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Inventory.instance.HideTooltip();
    }
}
