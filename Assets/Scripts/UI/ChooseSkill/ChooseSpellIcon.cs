﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChooseSpellIcon : MonoBehaviour,IPointerDownHandler,IPointerEnterHandler,IPointerExitHandler
{

    public SpellType spellType;

    

    public void OnPointerDown(PointerEventData eventData)
    {
        SpellManager.spellManager.ChooseSpell(spellType, transform.GetChild(0).GetComponent<Image>().sprite);

        Inventory.instance.HideTooltip();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        string description = "";

        #region Decsription

        switch (spellType)
        {
            case SpellType.Heal:

                description = "<align=center>Heal Spell</align>" + "\n" + "Heals 50 points\nCooldown:10 sec";

                break;
            case SpellType.Grenade:

                description = "<align=center>Grenade Spell</align>" + "\n" + "Aka alahua akbar BOOM!";

                break;
            case SpellType.Shield:

                description = "<align=center>Shield Spell</align>" + "\n" + "Creates a shield for 5 seconds\nCooldown:18 sec";

                break;
        }


        #endregion


        Inventory.instance.ShowTooltip(description);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Inventory.instance.HideTooltip();
    }
}
