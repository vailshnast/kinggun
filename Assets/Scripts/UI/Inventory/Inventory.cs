﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.EventSystems;

public class Inventory : MonoBehaviour
{
    public TextMeshProUGUI parametersText;
    public GameObject parameterPanel;
    public TextMeshProUGUI creditsText;
    public static Inventory instance;

    public Transform panel;
    public GameObject stats;

    public List<ItemInfoTemplate> itemTemplates;

    private List<ItemSlot> inventorySlots;
    private List<ModuleSlot> moduleSlots;

    public GameObject tooltip;
    public TextMeshProUGUI tooltipText;

    private Canvas canvas;
    private RectTransform canvasRect;
    private bool updateTooltipPos;

    private ItemDragHandler selectedItem;

    public PlayerInventoryData data;

    void Awake()
    {
        instance = this;
        inventorySlots = new List<ItemSlot>();
        moduleSlots = new List<ModuleSlot>();
        canvas = FindObjectOfType<Canvas>();
        canvasRect = canvas.GetComponent<RectTransform>();
    }

    void Start()
    {
        InitializeSlots();

        AddItemToInventory("Pistol");
        AddItemToInventory("Energy Block");
        AddItemToInventory("Armour");

        /*AddItemToInventory("Pistol", 10);

        AddItemToInventory("Rifle");
        AddItemToInventory("Rifle",10);

        AddItemToInventory("ShotGun");
        AddItemToInventory("ShotGun", 10);

        AddItemToInventory("SniperRifle");
        AddItemToInventory("SniperRifle", 10);*/

        //LoadInventory();

        if (data.newGame)
        {
            /*for (int i = 0; i < noobPack.Count; i++)
            {
                AddItemToInventory(new ItemInfo(noobPack[i],0));
            }
            data.newGame = false;*/
        }
        data.credits = 0;
        UpdateUi();
    }

    void Update()
    {
        UpdateTooltipPos();
    }

    private void UpdateTooltipPos()
    {
        if (updateTooltipPos)
        {
            Vector2 pos = Input.mousePosition / canvas.scaleFactor - new Vector3(
                              canvasRect.sizeDelta.x / 2,
                              canvasRect.sizeDelta.y / 2);

            if (pos.x + tooltip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x / 2 >= canvasRect.sizeDelta.x / 2)
                pos.x -= tooltip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x / 2;
            else if (pos.x - tooltip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x / 2 <= -canvasRect.sizeDelta.x / 2)
                pos.x += tooltip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x / 2;

            //UEBAK DA TI JELTIY
            //if (pos.y + tooltip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y >= canvasRect.sizeDelta.y / 2)
              //  pos.y -= tooltip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y ;


             if (pos.y - tooltip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y <= -canvasRect.sizeDelta.y/2 )
                pos.y += tooltip.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.y ;


            tooltip.GetComponent<RectTransform>().anchoredPosition = pos;

        }
    }

    public void AddCredit(int amount)
    {
        data.credits += amount;
        creditsText.text = "Credits: " + data.credits;
    }

    public void UpdateUi()
    {

        int energyAmount = 30;

        parametersText.text =
            "<align=center>Parameters</align>" + "\n" +
            "Health: " + PlayerStats.instance.GetStatAmount(PropertyType.Health) + "\n" +
            "Energy: " + PlayerStats.instance.GetStatAmount(PropertyType.Energy) + "\n" +
            "Health Regen: " + string.Format("{0:0.0}", PlayerStats.instance.GetStatAmount(PropertyType.HealthRegen)) + "\n" +
            "Energy Regen: " + string.Format("{0:0.0}", PlayerStats.instance.GetStatAmount(PropertyType.EnergyRegen)) + "\n" +
            "SkillPower: " + PlayerStats.instance.GetStatAmount(PropertyType.SkillPower) + "%\n" +
            "Ammo: " + PlayerStats.instance.GetStatAmount(PropertyType.Ammo) + "\n" +
            "Armor: " + PlayerStats.instance.GetStatAmount(PropertyType.Armor) + "\n" +
            "Speed: " + string.Format("{0:0.0}", PlayerStats.instance.GetStatAmount(PropertyType.Speed)) + "\n" +
            "Damage: " + PlayerStats.instance.GetStatAmount(PropertyType.Damage) + "\n" +
            "Fire Rate: " + (int) PlayerStats.instance.GetStatAmount(PropertyType.FireRate) + "\n";

        creditsText.text = "Credits: " + data.credits;
    }

    private void InitializeSlots()
    {
        for (int i = 0; i < panel.childCount; i++)
        {
            if (panel.GetChild(i).GetComponent<ItemSlot>() != null)
            {
                inventorySlots.Add(panel.GetChild(i).GetComponent<ItemSlot>());
                panel.GetChild(i).GetComponent<ItemSlot>().ItemDragHandler.InitializeSlot();
            }

            if (panel.GetChild(i).GetComponent<ModuleSlot>() != null)
            {
                moduleSlots.Add(panel.GetChild(i).GetComponent<ModuleSlot>());
                panel.GetChild(i).GetComponent<ModuleSlot>().ItemDragHandler.InitializeSlot();
            }
                
        }
    }    

    public void SaveInventory()
    {
        data.playerInventoryItemList = new List<ItemInfo>();
        data.slotItemList = new List<InventorySlotData>();

        for (int i = 0; i < moduleSlots.Count; i++)
        {
            ItemInfo itemInfo = moduleSlots[i].ItemDragHandler.itemInfo;

            if (itemInfo != null)
            {
                data.slotItemList.Add(new InventorySlotData(itemInfo, moduleSlots[i].itemType));
            }
        }

        for (int i = 0; i < inventorySlots.Count; i++)
        {
            ItemInfo itemInfo = inventorySlots[i].ItemDragHandler.itemInfo;
            if (itemInfo != null)
            {
                itemInfo.SetInventoryID(i);
                data.playerInventoryItemList.Add(itemInfo);
            }

        }
        //data.credits = MainMenu.instance.credits;
    }

    private void LoadInventory()
    {
        PlayerStats.instance.ResetStats();

        if (data.playerIsDead)
        {
            data.slotItemList = new List<InventorySlotData>();
            data.playerIsDead = false;
        }

        for (int i = 0; i < data.playerInventoryItemList.Count; i++)
        {
            AddItemToInventory(data.playerInventoryItemList[i], data.playerInventoryItemList[i].itemInventoryID,false);
        }

        for (int i = 0; i < data.slotItemList.Count; i++)
        {
            ModuleSlot slot = moduleSlots.FirstOrDefault(x => x.itemType == data.slotItemList[i].slotType && x.ItemDragHandler.itemInfo == null);
            slot.ItemDragHandler.SetUpItem(data.slotItemList[i].itemInfo);
            slot.ItemDragHandler.AddItemProperties();
        }
    }

    public bool AddItemToInventory(ItemInfo item)
    {
        ItemSlot emptyItemSlot = inventorySlots.FirstOrDefault(x => x.ItemDragHandler.IsEmpty);

        if(emptyItemSlot== null)
            return false;

        emptyItemSlot.ItemDragHandler.SetUpItem(item);

        SaveInventory();
        return true;
    }

    public void AddItemToInventory(ItemInfo item, int index, bool save= true)
    {
        ItemSlot itemSlot = inventorySlots[index];

        itemSlot.ItemDragHandler.SetUpItem(item);
        if(save)
        SaveInventory();
    }

    public void AddItemToInventory(string name, int itemLevel = 1)
    {
        ItemInfoTemplate itemTemplate = itemTemplates.FirstOrDefault(x => x.itemName == name);

        ItemSlot emptyItemSlot = inventorySlots.FirstOrDefault(x => x.ItemDragHandler.IsEmpty);

        emptyItemSlot.ItemDragHandler.SetUpItem(itemTemplate, itemLevel);

        SaveInventory();
    }

    public ItemInfo GetItem(ItemType type)
    {
        ModuleSlot slot = moduleSlots.FirstOrDefault(x => x.itemType == type);

        if (!slot.ItemDragHandler.IsEmpty)
            return slot.ItemDragHandler.itemInfo;

        return null;
    }

    public ModuleSlot GetModuleSlot(ItemType type)
    {
        return moduleSlots.FirstOrDefault(x => x.itemType == type);
    }

    public void ShowTooltip(ItemInfo itemInfo, bool showCredits)
    {
        tooltip.SetActive(true);

        updateTooltipPos = true;

        //tooltip.transform.position = Input.mousePosition;

        string text = "<align=center>" + itemInfo.itemName + " </align>" + "\n";


        for (int i = 0; i < itemInfo.propertyInfos.Count; i++)
        {

            if (itemInfo.propertyInfos[i].isStatic && itemInfo.propertyInfos[i].propertyType != PropertyType.Spread)
                continue;

            if (itemInfo.propertyInfos[i].propertyType == PropertyType.HealthRegen || itemInfo.propertyInfos[i].propertyType == PropertyType.EnergyRegen || itemInfo.propertyInfos[i].propertyType == PropertyType.ReloadTime)
                text += itemInfo.propertyInfos[i].propertyType + ": " + string.Format("{0:0.0}", itemInfo.propertyInfos[i].propertyAmount) + "\n";
            else if(itemInfo.propertyInfos[i].propertyType == PropertyType.SkillPower)
                text += itemInfo.propertyInfos[i].propertyType + ": " + Mathf.Ceil(itemInfo.propertyInfos[i].propertyAmount) + "%\n";
            else
                text += itemInfo.propertyInfos[i].propertyType + ": " + Mathf.Ceil(itemInfo.propertyInfos[i].propertyAmount) + "\n";
        }

        text += "Item Level: " + itemInfo.levelRequirement;

        if (showCredits)
        {
            text += "\nCredits: " + itemInfo.GetBuyPrice();
        }
        else
        {
            text += "\nSell Price: " + itemInfo.GetSellPrice();
        }

        tooltipText.text = text;
    }

    public void ShowTooltip(string text)
    {
        tooltipText.text = text;

        tooltip.SetActive(true);

        updateTooltipPos = true;
    }

    public void HideTooltip()
    {
        tooltip.SetActive(false);
        updateTooltipPos = false;
    }

    public void SaveCredits()
    {
        //data.credits = MainMenu.instance.credits;
    }

    public void ToggleStats()
    {
        if (panel.gameObject.activeInHierarchy)
        {
            stats.SetActive(!stats.activeInHierarchy);
        }
    }

    public void SetSelectedItem(ItemDragHandler selected)
    {
        selectedItem = selected;
    }

    public void RemoveSelection()
    {
        selectedItem = null;
    }

    public void ToggleInventory()
    {
        if(selectedItem != null)
            selectedItem.ReturnItemToStartParent();

        transform.GetChild(0).gameObject.SetActive(!transform.GetChild(0).gameObject.activeInHierarchy);

        if(!transform.GetChild(0).gameObject.activeInHierarchy)
            HideTooltip();


    }
}

