﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ItemDragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler , IPointerEnterHandler, IPointerExitHandler,IPointerDownHandler
{
    //public ItemInfo itemInfo;
    [HideInInspector]
    public Transform startParent;
    private Vector3 startDragPos;

    private CanvasGroup canvasGroup;
    [HideInInspector]
    public ItemInfo itemInfo { get; private set; }
    [HideInInspector]
    public bool IsEmpty;

    private Image imageComponent;
    
    public void InitializeSlot()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        startParent = transform.parent;
        imageComponent = GetComponent<Image>();
        itemInfo = null;
        IsEmpty = true;
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        Inventory.instance.SetSelectedItem(this);

        startDragPos = transform.position;
        startParent = transform.parent;
        startParent.SetAsLastSibling();

        if (startParent.GetComponent<ModuleSlot>() != null && !IsEmpty)
        {
            RemoveItemProperties();
        }

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        if (results.Count > 0)
        {
            if (results[0].gameObject.GetComponent<ItemSlot>() != null)
            {
                Debug.Log("slot");
            }


        }

        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (startParent == transform.parent)
        {
            if (startParent.GetComponent<ModuleSlot>() != null && !IsEmpty)
            {
                AddItemProperties();
            }

            transform.position = startDragPos;
        }

        Inventory.instance.UpdateUi();
        canvasGroup.blocksRaycasts = true;
        Inventory.instance.RemoveSelection();
    }

    public void ReturnItemToStartParent()
    {
        if (startParent == transform.parent)
        {
            if (startParent.GetComponent<ModuleSlot>() != null && !IsEmpty)
            {
                AddItemProperties();
            }

            transform.position = startDragPos;
        }

        Inventory.instance.UpdateUi();
        canvasGroup.blocksRaycasts = true;
        Inventory.instance.RemoveSelection();
    }

    public void SetUpItem(ItemInfoTemplate template, int itemLevel = 1)
    {
        IsEmpty = false;

        itemInfo = new ItemInfo(template, itemLevel);

        imageComponent.sprite = itemInfo.itemSprite;

        imageComponent.enabled = true;
    }

    public void RemoveItem()
    {
        IsEmpty = true;
        itemInfo = null;
        imageComponent.enabled = false;
    }

    public void AddItemProperties(bool reload = true)
    {
        for (int i = 0; i < itemInfo.propertyInfos.Count; i++)
        {
            PlayerStats.instance.ChangeStat(itemInfo.propertyInfos[i].propertyType, itemInfo.propertyInfos[i].propertyAmount);
        }

        if(itemInfo.type == ItemType.Weapon && reload)
            Player.player.Reload();
        
        Inventory.instance.UpdateUi();
    }

    public void SetItemProperties()
    {
        for (int i = 0; i < itemInfo.propertyInfos.Count; i++)
        {
            PlayerStats.instance.SetStat(itemInfo.propertyInfos[i].propertyType, itemInfo.propertyInfos[i].propertyAmount);
        }
        Inventory.instance.UpdateUi();
    }

    public void RemoveItemProperties(bool reload = true)
    {
        for (int i = 0; i < itemInfo.propertyInfos.Count; i++)
        {
            PlayerStats.instance.ChangeStat(itemInfo.propertyInfos[i].propertyType, -itemInfo.propertyInfos[i].propertyAmount);
        }

        if (itemInfo.type == ItemType.Weapon && reload)
            Player.player.Reload();

        Inventory.instance.UpdateUi();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(!IsEmpty)
        Inventory.instance.ShowTooltip(itemInfo,false);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Inventory.instance.HideTooltip();
    }

    public void SetUpItem(ItemInfo template)
    {
        IsEmpty = false;

        itemInfo = template;        
        
        imageComponent.sprite = itemInfo.itemSprite;

        imageComponent.enabled = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (Shop.instance.isOpened && transform.parent.GetComponent<ModuleSlot>() == null)
            {
                Inventory.instance.AddCredit(itemInfo.GetSellPrice());
                RemoveItem();
            }
            else
            {
                if (transform.parent.GetComponent<ModuleSlot>() == null)
                {
                    RemoveItem();
                }
            }

          
        }
    }
}

