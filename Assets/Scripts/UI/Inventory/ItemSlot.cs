﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour,IDropHandler
{
    public ItemDragHandler ItemDragHandler
    {
        get { return transform.GetComponentInChildren<ItemDragHandler>(); }
    }

    public void OnDrop(PointerEventData eventData)
    {
        
        Transform currentSlotItem = transform.GetChild(0);
        ItemDragHandler dragItemDragHandler = eventData.pointerDrag.GetComponent<ItemDragHandler>();
        Transform previousParent = dragItemDragHandler.startParent;

        if (previousParent.GetComponent<ModuleSlot>() != null)
        {
            if(!ItemDragHandler.IsEmpty && ItemDragHandler.itemInfo.type != dragItemDragHandler.itemInfo.type)
                return;
            else if (!ItemDragHandler.IsEmpty)
            {
                ItemDragHandler.AddItemProperties();
                Inventory.instance.UpdateUi();
            }
                
            
        }

        currentSlotItem.SetParent(previousParent);
        currentSlotItem.position = previousParent.position;
        eventData.pointerDrag.transform.SetParent(transform);
        eventData.pointerDrag.transform.position = transform.position;
    }
}
