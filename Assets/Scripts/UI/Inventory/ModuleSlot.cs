﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ModuleSlot : MonoBehaviour, IDropHandler
{
    public ItemType itemType;

    public ItemDragHandler ItemDragHandler
    {
        get { return transform.GetComponentInChildren<ItemDragHandler>(); }
    }

    public void OnDrop(PointerEventData eventData)
    {
        Transform currentSlotItem = transform.GetChild(0);

        ItemDragHandler draggItemDragHandler = eventData.pointerDrag.GetComponent<ItemDragHandler>();

        Transform previousParent = draggItemDragHandler.startParent;

        if (draggItemDragHandler.itemInfo.type == itemType && draggItemDragHandler.itemInfo.levelRequirement <= Player.player.level)
        {
            if (draggItemDragHandler != ItemDragHandler)
                draggItemDragHandler.AddItemProperties();

            if (!ItemDragHandler.IsEmpty && ItemDragHandler != draggItemDragHandler)
                ItemDragHandler.RemoveItemProperties();

            if (!ItemDragHandler.IsEmpty && draggItemDragHandler.startParent.GetComponent<ModuleSlot>() == null)
                ItemDragHandler.RemoveItemProperties();

            if (ItemDragHandler.itemInfo != null && currentSlotItem.parent == transform && draggItemDragHandler != ItemDragHandler)
                ItemDragHandler.AddItemProperties();
        }
        else
            return;

        Inventory.instance.UpdateUi();

        currentSlotItem.SetParent(previousParent);
        currentSlotItem.position = previousParent.position;
        eventData.pointerDrag.transform.SetParent(transform);
        eventData.pointerDrag.transform.position = transform.position;
    }

}
