﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PlayerData", menuName = "Inventory/PlayerData", order = 1)]
public class PlayerInventoryData : ScriptableObject
{

    public List<InventorySlotData> slotItemList;
    public List<ItemInfo> playerInventoryItemList;
    public int credits;
    public int level;
    public int experience;

    public bool newGame;
    public bool playerIsDead;

    
}

[System.Serializable]
public struct InventorySlotData
{
    public ItemInfo itemInfo;
    public ItemType slotType;

    public InventorySlotData(ItemInfo itemInfo, ItemType slotType)
    {
        this.itemInfo = itemInfo;
        this.slotType = slotType;
    }
}

