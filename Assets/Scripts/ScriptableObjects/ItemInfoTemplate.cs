﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

#region ScriptableObject

[CreateAssetMenu(fileName = "ItemTemplate", menuName = "Inventory/Item", order = 1)]
public class ItemInfoTemplate : ScriptableObject
{
    public string itemName;

    public string itemDescription;

    public Sprite itemSprite;

    public ItemType type;

    [ShowIf("type", ItemType.Weapon)]
    public FireType fireType;

    [ShowIf("type", ItemType.Weapon)]
    public WeaponType weaponType;
    [ShowIf("type", ItemType.Weapon)]
    public AudioClip weaponSound;

    [SerializeField]
    public List<ItemPropertyInfo> propertyInfos;
}

[System.Serializable]
public class ItemInfo
{
    public string itemName;

    public string itemDescription;

    public Sprite itemSprite;

    public FireType fireType;

    public int levelRequirement;

    public WeaponType weaponType;

    public AudioClip weaponSound;

    public ItemType type;

    public int itemInventoryID { get; private set; }

    [SerializeField]
    public List<ItemPropertyInfo> propertyInfos;

    public ItemInfo(ItemInfoTemplate template,int levelRequirement)
    {
        itemName = template.itemName;
        itemDescription = template.itemDescription;
        itemSprite = template.itemSprite;
        type = template.type;
        fireType = template.fireType;
        weaponType = template.weaponType;
        weaponSound = template.weaponSound;
        propertyInfos = template.propertyInfos.Select(t => new ItemPropertyInfo(t)).ToList();

        this.levelRequirement = levelRequirement;
        InitializeRandomProperties(levelRequirement);
    }

    public ItemInfo(ItemInfo itemInfo)
    {
        itemName = itemInfo.itemName;
        itemDescription = itemInfo.itemDescription;
        itemSprite = itemInfo.itemSprite;
        fireType = itemInfo.fireType;
        levelRequirement = itemInfo.levelRequirement;
        weaponType = itemInfo.weaponType;
        type = itemInfo.type;
        weaponSound = itemInfo.weaponSound;
        propertyInfos = itemInfo.propertyInfos.Select(t => new ItemPropertyInfo(t)).ToList();
    }
   
    /*
        public ItemInfo(ModuleInfoTemplate template)
        {
            itemName = template.itemName;
            itemDescription = template.itemDescription;
            itemSprite = template.itemSprite;
            type = template.type;
            propertyInfos = new List<ItemPropertyInfo>();
            InitializeModuleProperties(template.propertiesAmount,template.propertyInfos);
        }

        private void InitializeModuleProperties(int propertiesAmount, List<ItemPropertyInfo> propertiesToRandomFrom)
        {
            List<ItemPropertyInfo> propertiesCache = propertiesToRandomFrom.Select(t => new ItemPropertyInfo(t)).ToList();

            ItemPropertyInfo energyCellConsume = propertiesCache.FirstOrDefault(x => x.propertyType == PropertyType.EnergyCellsConsumeAmount);

            propertiesCache.Remove(energyCellConsume);

            energyCellConsume.RandomPropertyAmount();

            for (int i = 0; i < propertiesAmount; i++)
            {
                ItemPropertyInfo propertyInfo = propertiesCache[Random.Range(0, propertiesCache.Count)];
                propertiesCache.Remove(propertyInfo);

                propertyInfo.RandomPropertyAmount();

                propertyInfos.Add(propertyInfo);
            }
            propertyInfos.Add(energyCellConsume);
        }
    */

    public int GetBuyPrice()
    {
        return levelRequirement * 1000;
    }

    public int GetSellPrice()
    {
        return GetBuyPrice() / 3;
    }

    public ItemPropertyInfo GetStatAmount(PropertyType statType)
    {
        ItemPropertyInfo stat = propertyInfos.FirstOrDefault(x => x.propertyType == statType);

        return stat;
    }

    public void SetInventoryID(int id)
    {
        itemInventoryID = id;
    }

    private void InitializeRandomProperties(int levelRequirement)
    {
        List<ItemPropertyInfo> randomPropertiesList = propertyInfos.Where(t => !t.isStatic).ToList();

        if (type == ItemType.Module)
        {          
            for (int i = 4 - 1; i >= 0; i--)
            {
                randomPropertiesList.RemoveAt(Random.Range(0, randomPropertiesList.Count));
            }
            propertyInfos = randomPropertiesList;
        }

        if (type == ItemType.Weapon)
        {
            ItemPropertyInfo spread = propertyInfos.FirstOrDefault(x => x.propertyType == PropertyType.Spread);
            if(!spread.isStatic)
            spread.propertyAmount = Random.Range(20, 40);
        }
        
        for (int i = 0; i < randomPropertiesList.Count; i++)
        {
            float totalValue = 0;
            float minRange = 0;
            float maxRange = 0;
            bool round = true;

            #region PropertyType

            switch (randomPropertiesList[i].propertyType)
            {
                case PropertyType.Health:

                    minRange = 20 + levelRequirement * 5;
                    maxRange = 65 + levelRequirement * 10;

                    break;
                case PropertyType.HealthRegen:

                    minRange = 0.5f + levelRequirement * 0.1f;
                    maxRange = 1.5f + levelRequirement * 0.1f;

                    round = false;

                    break;
                case PropertyType.Armor:

                    minRange = 5 + levelRequirement * 1;
                    maxRange = 10 + levelRequirement * 2;

                    break;

                #region Weapon

                case PropertyType.Damage:

                    switch (weaponType)
                    {
                        case WeaponType.Rifle:

                            minRange = 10 + levelRequirement * 2;
                            maxRange = 18 + levelRequirement * 2;

                            break;
                        case WeaponType.ShotGun:

                            minRange = 8 + levelRequirement * 1;
                            maxRange = 14 + levelRequirement * 2;

                            break;
                        case WeaponType.Pistol:

                            minRange = 20 + levelRequirement * 1;
                            maxRange = 25 + levelRequirement * 3;

                            break;

                        case WeaponType.SniperRifle:

                            minRange = 100 + levelRequirement * 10;
                            maxRange = 130 + levelRequirement * 20;

                            break;
                    }

                    break;

                case PropertyType.ReloadTime:

                    switch (weaponType)
                    {
                        case WeaponType.Rifle:

                            minRange = 1f;
                            maxRange = 2.5f;

                            break;
                        case WeaponType.ShotGun:

                            minRange = 1.5f;
                            maxRange = 2.7f;

                            break;
                        case WeaponType.Pistol:

                            minRange = 0.5f;
                            maxRange = 1.2f;

                            break;

                        case WeaponType.SniperRifle:

                            minRange = 1;
                            maxRange = 3;

                            break;
                    }

                    round = false;

                    break;

                case PropertyType.Ammo:

                    switch (weaponType)
                    {
                        case WeaponType.Rifle:

                            minRange = 10;
                            maxRange = 35;

                            break;
                        case WeaponType.ShotGun:

                            minRange = 2;
                            maxRange = 8;

                            break;
                        case WeaponType.Pistol:

                            minRange = 8;
                            maxRange = 20;

                            break;

                        case WeaponType.SniperRifle:

                            minRange = 1;
                            maxRange = 5;

                            break;
                    }

                    break;

                    

                #endregion
                case PropertyType.Energy:

                    minRange = 25 + levelRequirement * 3;
                    maxRange = 75 + levelRequirement * 5;

                    break;

                case PropertyType.SkillPower:

                    minRange = 10 + levelRequirement * 1;
                    maxRange = 20 + levelRequirement * 2;

                    break;
                case PropertyType.EnergyRegen:

                    minRange = 1.5f + levelRequirement * 0.1f;
                    maxRange = 3 + levelRequirement * 0.2f;

                    round = false;

                    break;
                case PropertyType.EnergyConsume:

                    minRange = 5 + levelRequirement * 2;
                    maxRange = 15 + levelRequirement * 7;

                    break;
                case PropertyType.FireRate:

                    minRange = 300;
                    maxRange = 750;

                    break;
                case PropertyType.BulletCount:

                    minRange = 5 + levelRequirement * 2;
                    maxRange = 15 + levelRequirement * 7;

                    break;
                case PropertyType.Cooling:

                    minRange = 8 + levelRequirement * 2;
                    maxRange = 25 + levelRequirement * 2;

                    break;
                case PropertyType.HeatPool:

                    minRange = 25 + levelRequirement * 2;
                    maxRange = 40 + levelRequirement * 3;

                    break;
            }

                #endregion
        
            totalValue = Random.Range(minRange, maxRange);

            if (round)
                totalValue = Mathf.Ceil(totalValue);

            randomPropertiesList[i].propertyAmount += totalValue;
        }
    }
}
[System.Serializable]
public class ItemPropertyInfo
{
    public PropertyType propertyType;

    public float propertyAmount;

    public bool isStatic;
    public ItemPropertyInfo(ItemPropertyInfo itemPropertyInfo)
    {
        propertyType = itemPropertyInfo.propertyType;
        isStatic = itemPropertyInfo.isStatic;
        propertyAmount = itemPropertyInfo.propertyAmount;
    }

}
public enum ItemType
{
    Weapon,
    BodyArmour,
    EnergyBlock,
    Processor,
    Module,
    Skill
}
public enum PropertyType
{
    Health,
    HealthRegen,
    Ammo,
    Range,
    Armor,
    Speed,
    Damage,
    Energy,
    EnergyRegen,
    EnergyConsume,
    Spread,
    FireRate,
    ReloadTime,
    BulletCount,

    DashCharges,
    DashCooldown,

    Cooling,
    DashCooling,

    HeatPool,
    SkillPower
}
public enum WeaponType
{
    Rifle,
    ShotGun,
    Pistol,
    SniperRifle
}
public enum SpellType
{
    Heal,
    Grenade,
    Shield
}
public enum FireType
{
    Semi,
    Auto
}
#endregion
