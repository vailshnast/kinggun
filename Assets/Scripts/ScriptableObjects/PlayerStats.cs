﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class PlayerStats : MonoBehaviour
{

    // global point of access
    public static PlayerStats instance;

    public List<PlayerStat> stats;


    void Awake()
    {
        if (instance == null)
        {
            //DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            if (instance != this)
            {
                Destroy(gameObject);
            }
        }
    }

    public void ResetStats()
    {
        for (int i = 0; i < stats.Count; i++)
        {
            stats[i].amount = 0;
        }
    }

    public void ChangeStat(PropertyType statType,float amount)
    {
        PlayerStat stat = stats.FirstOrDefault(x => x.type == statType);
        stat.amount += amount;
        Inventory.instance.UpdateUi();
    }

    public void SetStat(PropertyType statType, float amount)
    {
        PlayerStat stat = stats.FirstOrDefault(x => x.type == statType);
        stat.amount = amount;
        Inventory.instance.UpdateUi();
    }

    public void AddItemProperties(ItemInfo itemInfo)
    {
        for (int i = 0; i < itemInfo.propertyInfos.Count; i++)
        {
            instance.ChangeStat(itemInfo.propertyInfos[i].propertyType, itemInfo.propertyInfos[i].propertyAmount);
        }

        Inventory.instance.UpdateUi();
    }

    public void SetItemProperties(ItemInfo itemInfo)
    {
        for (int i = 0; i < itemInfo.propertyInfos.Count; i++)
        {
            instance.SetStat(itemInfo.propertyInfos[i].propertyType, itemInfo.propertyInfos[i].propertyAmount);
        }
        Inventory.instance.UpdateUi();
    }

    public void RemoveItemProperties(ItemInfo itemInfo)
    {
        for (int i = 0; i < itemInfo.propertyInfos.Count; i++)
        {
            instance.ChangeStat(itemInfo.propertyInfos[i].propertyType, -itemInfo.propertyInfos[i].propertyAmount);
        }
        Inventory.instance.UpdateUi();
    }

    public float GetStatAmount(PropertyType statType)
    {
        PlayerStat stat = stats.FirstOrDefault(x => x.type == statType);
        
        if(stats.FirstOrDefault(x => x.type == statType)==null)
            Debug.Log(statType);

        return stat.amount;
    }

    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }
}

[System.Serializable]
public class PlayerStat
{
    public PropertyType type;
    public float amount;



    public PlayerStat(PropertyType type, float amount)
    {
        this.type = type;
        this.amount = amount;
    }
}