﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Icon", menuName = "Map/Icon", order = 1)]
public class MapIconTemplate : ScriptableObject
{
    public Vector2 size;
    public Sprite sprite;
    public GroupType groupType;
    public IconType iconType;
}
