﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour
{

    public static EnemySpawner instance;


    public GameObject enemy;
    public int count;

    public GameObject lootBox;
    public int lootBoxCount;

    public float delayOnEnter = 10;

    void Awake()
    {
        instance = this;
        
    }

    void Start()
    {
/*        for (int i = 0; i < lootBoxCount; i++)
        {
            AddLootBox();
        }*/
    }



    public void AddLootBox()
    {
        Vector3 randomPoint = new Vector3(Random.Range(-70, 0), 0, Random.Range(0, 70));

        NavMeshHit hit;
        NavMesh.SamplePosition(randomPoint, out hit, 2, 1);


        LootBox loot = Instantiate(lootBox, hit.position, Quaternion.identity).GetComponent<LootBox>();
        loot.RandomItem(Mathf.Clamp(Random.Range(Player.player.level - 2, Player.player.level + 2), 1, 1000));
    }

    public Transform AddLootBox(Vector3 pos, bool checkForNavmesh = true)
    {
        Vector3 point = pos;

        NavMeshHit hit;
        NavMesh.SamplePosition(point, out hit, 2, 1);

        Vector3 resultPos = pos;

        if (checkForNavmesh)
            resultPos = hit.position;

        LootBox loot = Instantiate(lootBox, resultPos, Quaternion.identity).GetComponent<LootBox>();
        loot.RandomItem(Mathf.Clamp(Random.Range(Player.player.level - 2, Player.player.level + 2), 1, 1000));

        return loot.transform;
    }



    public void ConstructEnemy()
    {
        Instantiate(enemy, new Vector3(Random.Range(-70, 0), 0, Random.Range(0, 70)), Quaternion.identity, GameObject.Find("Enemies").transform);
    }

    public void StartEnemySpawn()
    {
        StopAllCoroutines();
        StartCoroutine(StartSpawnEnemyCoroutine());
    }

    public void StopEnemySpawn()
    {
        StopAllCoroutines();

        DeleteEnemies();
    }

    private void DeleteEnemies()
    {
        for (int j = transform.childCount - 1; j >= 0; j--)
        {
            transform.GetChild(j).GetChild(0).GetComponent<MapMarker>().RemoveMarker();
            Destroy(transform.GetChild(j).gameObject);
        }
    }

  

    public void SpawnEnemy()
    {
        StartCoroutine(SpawnEnemyWithDelayCoroutine());
    }

    private IEnumerator StartSpawnEnemyCoroutine()
    {
        yield return new WaitForSeconds(delayOnEnter);
        for (int i = 0; i < count + Player.player.level / 2; i++)
        {
            ConstructEnemy();
        }
    }
    private IEnumerator SpawnEnemyWithDelayCoroutine()
    {
        yield return new WaitForSeconds(15);
        ConstructEnemy();
    }

}
