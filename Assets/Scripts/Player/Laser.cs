﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{

    public float scrollSpeed;
    public float pulseSpeed;

    public float minWidth;
    public float maxWidth;
    public float length;
    public float height = 5;

    private float aniDir = 1;

    public GameObject dot;

    public float angleRotationRange = 30;
    public float rotationSpeed = 1;

    public int damage = 1;
    public float hitRate = 0.3f;

    private Material mat;
    private LineRenderer lineRenderer;

    private bool isLaserOnCooldown;

    private float currentAngle;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        mat = lineRenderer.material;
        currentAngle = 0;
    }

	// Update is called once per frame
	void Update () {

	    //AnimateLaser();
	    //currentAngle = PingPong(Time.time * rotationSpeed, -angleRotationRange, angleRotationRange);

        SetLaserPos();
    }

    private void AnimateLaser()
    {
        //mat.mainTextureOffset = new Vector2(Time.deltaTime * aniDir * scrollSpeed + mat.mainTextureOffset.x, 0);
        //mat.SetTextureOffset("_NoiseTex", new Vector2(-Time.time * aniDir * scrollSpeed, 0.0f));

        float aniFactor = Mathf.PingPong(Time.time * pulseSpeed, 1.0f);
        aniFactor = Mathf.Max(minWidth, aniFactor) * maxWidth;
        lineRenderer.SetWidth(aniFactor, aniFactor);
    }

    /*private void SetLaserPos()
    {


        Vector3 endPosition = transform.position - length * transform.forward + new Vector3(0, -height, 0);

        Ray ray = new Ray(transform.position, (endPosition - transform.position).normalized);
        RaycastHit raycastHit;

        if (Physics.Raycast(ray, out raycastHit))
        {
            if (raycastHit.collider.name == "Player")
            {
                if (!isLaserOnCooldown)
                    StartCoroutine(DealDamageToPlayer());
            }

            endPosition = raycastHit.point;
            //endPosition = new Vector3(Player.player.transform.position.x,transform.position.y,Player.player.transform.position.z);
            
            dot.transform.position = endPosition;
        }

        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, endPosition);


    }*/
    private void SetLaserPos()
    {
        Vector3 endPosition = transform.position - length * transform.forward + new Vector3(0, -height, 0);

        Ray ray = new Ray(transform.position, (endPosition - transform.position).normalized);
        RaycastHit raycastHit;


        if (Physics.Raycast(ray, out raycastHit))
        {
            if (raycastHit.collider.name == "Player")
            {
                if (!isLaserOnCooldown)
                    StartCoroutine(DealDamageToPlayer());
            }

            endPosition = raycastHit.point;
            //endPosition = new Vector3(Player.player.transform.position.x,transform.position.y,Player.player.transform.position.z);
            dot.SetActive(true);
            dot.transform.position = endPosition;
        }
        else
            dot.SetActive(false);

        lineRenderer.SetPosition(0, transform.position);
        lineRenderer.SetPosition(1, endPosition);


    }
    float PingPong(float aValue, float aMin, float aMax)
    {
        return Mathf.PingPong(aValue, aMax - aMin) + aMin;
    }
    private IEnumerator DealDamageToPlayer()
    {
        isLaserOnCooldown = true;

        Player.player.DealDamageToPlayer(damage);

        yield return new WaitForSeconds(hitRate);

        isLaserOnCooldown = false;     
    }

}
