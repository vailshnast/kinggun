﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Spider : MonoBehaviour
{
    public static Spider spider;
    [Header("Spider Parameters")]

    public float bodyRotationSpeed = 0.5f;

    public float gunsRotationSpeed = 5;

    public float bulletHellDuration;
    public float laserDuration;

    public Slider healthSlider;
 
    public int health = 100;
    private int maxHealth;

    [Header("Bullet Parameters")]

    public GameObject bullet;

    public float fireRate = 1f;

    public int bulletCount = 10;

    public float fireZoneAngleSize;

    public float fireZoneMoveSpeed;
    private float bulletSpeed;

    [Header("Rocket Parameters")]
    public GameObject rocket;

    public GameObject rocketEffect;
    public Transform rocketEffectSpawnPos;
    public GameObject teleport;
    public Transform lootPlace;
    public int count;

    public float rocketHeight = 25;

    public float rocketDelay;

    private bool isBulletGunOnCooldown;

    private List<float> fireZonePositions;

    //References
    private GameObject body;
    private GameObject guns;
    ///

    ///State
    private bool changingPhasePending;

    private bool rotateLaser;

    private IEnumerator phaseChangerReference;

    //
    [Header("Phase Parameters")]
    public Phase currentPhase;
    public int currentPhaseDifficulty;
    private List<int> phaseThresholds;
    [Header("Sounds")]
    public AudioClip laserStart;
    public AudioClip bulletSound;
    public AudioClip rocketSound;

    private AudioSource audioSource;

    private bool died;
    public enum Phase
    {
        Laser,
        BulletHell,
        LasersAndBullets,

        Start
    }

    

    void Start()
    {
        spider = this;

        audioSource = GetComponent<AudioSource>();

        fireZonePositions = new List<float> { 0,90,180,270 };
        phaseThresholds = new List<int>() { 12000 , 7000 , 2000 };

        currentPhaseDifficulty = 0;
        
        body = transform.Find("Body").gameObject;
        guns = transform.Find("Guns").gameObject;
        
        maxHealth = health;
        bulletSpeed = 50;
        currentPhase = Phase.Start;
        rotateLaser = false;

        //ChangePhase(Phase.BulletHell);
    }
    void Update()
    {
        UpdatePhaseLogic();

    }
    [ContextMenu("ROck")]
    public void GetPosRock()
    {
        Debug.Log(rocketEffectSpawnPos.position);
    }

    public void ChangePhase(Phase phase)
    {
        if (currentPhase == phase)
        {
            Debug.Log("same state");
            return;
        }

        ExitPhase(currentPhase);
        currentPhase = phase;
        //Debug.Log(currentPhase);
        EnterPhase(phase);

    }

    private IEnumerator LaserSound()
    {
        AudioSource.PlayClipAtPoint(laserStart, Player.player.transform.position, 1);

        yield return new WaitForSeconds(1.1f);

        body.transform.GetChild(0).gameObject.SetActive(true);

        audioSource.enabled = true;
        audioSource.Play();
        rotateLaser = true;
    }

    public void ChangePhase(Phase phase, float seconds)
    {
        phaseChangerReference = ChangePhaseWithPause(phase, seconds);

        StartCoroutine(phaseChangerReference);
    }

    public void StopChangingPhase()
    {
        StopCoroutine(phaseChangerReference);
        changingPhasePending = false;
    }


    private IEnumerator ChangePhaseWithPause(Phase phase, float seconds)
    {
        if (changingPhasePending)
        {
            Debug.Log("changingPhasePending");
            yield break;
        }

        changingPhasePending = true;

        yield return new WaitForSeconds(seconds);       
        changingPhasePending = false;

        ChangePhase(phase);
    }

    private void ExitPhase(Phase phase)
    {

    }

    private void EnterPhase(Phase phase)
    {
        switch (phase)
        {
            case Phase.Laser:
               
                //body.transform.GetChild(0).gameObject.SetActive(true);
                ChangePhase(Phase.BulletHell, laserDuration);

                if(currentPhaseDifficulty == 1 || currentPhaseDifficulty == 2)
                StartCoroutine(SingleLaunch());

                StartCoroutine(LaserSound());
                break;
            case Phase.BulletHell:

                audioSource.Stop();
                rotateLaser = false;

                if (currentPhaseDifficulty == 2)
                    StartCoroutine(SingleLaunch());

                body.transform.GetChild(0).gameObject.SetActive(false);

                ChangePhase(Phase.Laser,bulletHellDuration);             

                break;

            case Phase.LasersAndBullets:

                audioSource.Play();

                StartCoroutine(MultiplyLauch());

                rotateLaser = false;

                StopChangingPhase();

                StartCoroutine(LaserSound());

                //body.transform.GetChild(0).gameObject.SetActive(true);

                break;
            case Phase.Start:
                break;
        }
    }


    public void DealDamage(int damage)
    {       
        health -= damage;

        for (int i = 0; i < phaseThresholds.Count; i++)
        {
            if (health < phaseThresholds[i])
                currentPhaseDifficulty = i + 1;
        }
        //firerate 0.1, speed = 50
        //firerate 0.075, speed = 50
        //firerat 0.075, speed = 60
        //firerate 0.060, speed = 60

        switch (currentPhaseDifficulty)
        {
            case 1:
                fireRate = 0.075f;
                bulletSpeed = 75;
                break;
            case 2:
                fireRate = 0.075f;
                bulletSpeed = 75;
                break;
            case 3:
                fireRate = 0.075f;
                bulletSpeed = 75;
                break;
        }


        if(currentPhaseDifficulty == 3 && currentPhase != Phase.LasersAndBullets)
            ChangePhase(Phase.LasersAndBullets);

        healthSlider.value = (float)health / maxHealth;
        if (health > 0) return;
        
        Death();
    }
    private IEnumerator SingleLaunch()
    {
        ////X(-156,-131) Y(-44,-20)
        for (int i = 0; i < count; i++)
        {
            Instantiate(rocket, new Vector3(Random.Range(-156, -131), rocketHeight, Random.Range(-44, -20)), Quaternion.identity);
            Instantiate(rocketEffect, new Vector3(Random.Range(-143.851f, -144.137f), 2.3f, Random.Range(-31.741f, -31.44f)), Quaternion.identity);
            if(i % 2 == 0)
            AudioSource.PlayClipAtPoint(rocketSound, Player.player.transform.position, 0.2f);

            yield return new WaitForSeconds(rocketDelay);
        }      
    }
    private IEnumerator MultiplyLauch()
    {

        for (int i = 0; i < count; i++)
        {
            Instantiate(rocket, new Vector3(Random.Range(-156, -131), rocketHeight, Random.Range(-44, -20)), Quaternion.identity);
            Instantiate(rocketEffect, new Vector3(Random.Range(-143.851f, -144.137f), 2.3f, Random.Range(-31.741f, -31.44f)), Quaternion.identity);
            if (i % 2 == 0)
                AudioSource.PlayClipAtPoint(rocketSound, Player.player.transform.position, 0.2f);
            yield return new WaitForSeconds(rocketDelay);
        }

        StartCoroutine(MultiplyLauch());

    }
    private void UpdatePhaseLogic()
    {

        switch (currentPhase)
        {
            case Phase.Laser:
                if(rotateLaser)
                body.transform.Rotate(0, 0, bodyRotationSpeed);

                break;
            case Phase.BulletHell:

                guns.transform.Rotate(0, 0, gunsRotationSpeed);

                if (isBulletGunOnCooldown == false)
                    StartCoroutine(Shoot());

                for (int i = 0; i < fireZonePositions.Count; i++)
                {
                    fireZonePositions[i] += fireZoneMoveSpeed;

                    if (fireZonePositions[i] > 360)
                        fireZonePositions[i] = 0;
                }
                break;
            case Phase.LasersAndBullets:

                guns.transform.Rotate(0, 0, gunsRotationSpeed);
                if(rotateLaser)
                body.transform.Rotate(0, 0, bodyRotationSpeed);

                if (isBulletGunOnCooldown == false)
                    StartCoroutine(Shoot());

                for (int i = 0; i < fireZonePositions.Count; i++)
                {
                    fireZonePositions[i] += fireZoneMoveSpeed;

                    if (fireZonePositions[i] > 360)
                        fireZonePositions[i] = 0;
                }


                break;

        }
    }
    

    private IEnumerator Shoot()
    {
        isBulletGunOnCooldown = true;
        AudioSource.PlayClipAtPoint(bulletSound, transform.position, 0.6f);
        for (int i = 0; i < bulletCount; i++)
        {
            
            EnemyBullet bulletObj = Instantiate(bullet, new Vector3(transform.position.x, 1.3f, transform.position.z), Quaternion.Euler(0, GetAngle(), 0)).GetComponent<EnemyBullet>();
            bulletObj.speed = bulletSpeed;
        }

        yield return new WaitForSeconds(fireRate);

        isBulletGunOnCooldown = false;
    }

    private float GetAngle()
    {
        int chosenGapIndex = Random.Range(0, fireZonePositions.Count);

        float minFireZoneAngle = fireZonePositions[chosenGapIndex];
        float maxFireZoneAngle = minFireZoneAngle + fireZoneAngleSize;   

        float angle = Random.Range(minFireZoneAngle, maxFireZoneAngle);


        #region Debug
        /*if (maxFireZoneAngle > 360)
         maxFireZoneAngle = 0 + maxFireZoneAngle - 360;*/

        //angle = maxFireZoneAngle > minFireZoneAngle ? Random.Range(maxFireZoneAngle, 360 + minFireZoneAngle) : Random.Range(minFireZoneAngle, maxFireZoneAngle);

        /*        if (minFireZoneAngle < maxFireZoneAngle)
                    angle = Random.Range(minFireZoneAngle, maxFireZoneAngle);
                else
                    angle = Random.Range(m)*/

        /*if (maxFireZoneAngle > minFireZoneAngle)
        {
            angle = Random.Range(maxFireZoneAngle, 360 + minFireZoneAngle);
            if (angle > 360)
                Debug.Log("minGap:  " + minFireZoneAngle + "\nmaxGap: " + maxFireZoneAngle + "\nchosen angle: " + (angle - 360));
            else
                Debug.Log("minGap:  " + minFireZoneAngle + "\nmaxGap: " + maxFireZoneAngle + "\nchosen angle: " + angle);
        }
        else
        {
            angle = Random.Range(minFireZoneAngle, maxFireZoneAngle);
            //Debug.Log("minGap:  " + minGapAngle + "\nmaxGap: " + maxGapAngle + "\nchosen angle: " + angle);
        }*/
            //////////////////////////////////////////////////////////////////////////////////////
        /*float minGapAngle = gapAnglePosition;
        float maxGapAngle = gapAnglePosition + gapAngleSize;

        if (maxGapAngle > 360)
            maxGapAngle = 0 + maxGapAngle - 360;

        float angle = 0;

        angle = maxGapAngle > minGapAngle ? Random.Range(maxGapAngle, 360 + minGapAngle) : Random.Range(minGapAngle, maxGapAngle);
         /*if (maxGapAngle > minGapAngle)
         {
             angle = Random.Range(maxGapAngle, 360 + minGapAngle);
             /*if (angle > 360)
                 Debug.Log("minGap:  " + minGapAngle + "\nmaxGap: " + maxGapAngle + "\nchosen angle: " + (angle - 360));
             else
                 Debug.Log("minGap:  " + minGapAngle + "\nmaxGap: " + maxGapAngle + "\nchosen angle: " + angle);#1#
         }
         else
         {
             angle = Random.Range(minGapAngle, maxGapAngle);
             //Debug.Log("minGap:  " + minGapAngle + "\nmaxGap: " + maxGapAngle + "\nchosen angle: " + angle);
         }
         */

        #endregion
        return angle;
    }

    public void Reset()
    {
        health = maxHealth;
        healthSlider.value = (float)health / maxHealth;
        StopAllCoroutines();
        changingPhasePending = false;
        currentPhaseDifficulty = 0;
        bulletSpeed = 50;
        isBulletGunOnCooldown = false;
        body.transform.GetChild(0).gameObject.SetActive(false);
        currentPhase = Phase.Start;
        teleport.gameObject.SetActive(false);
        gameObject.SetActive(true);
        audioSource.Stop();
        died = false;
        for (int i = 0; i < lootPlace.childCount; i++)
        {
            Destroy(lootPlace.GetChild(i).gameObject);
        }
    }


    private void Death()
    {
        if(died)
            return;;

        Player.player.AddExperience(3000);

        died = true;
        gameObject.SetActive(false);
        teleport.gameObject.SetActive(true);

        Vector3 pos;
        pos.y = transform.position.y;
        for (int i = 0; i < 10; i++)
        {
            pos = transform.position + Random.insideUnitSphere * 5;
            Transform loot = EnemySpawner.instance.AddLootBox(pos, false);
            loot.SetParent(lootPlace);
        }
    }


    void OnTriggerEnter(Collider coll)
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullets")
        {
            DealDamage(collision.gameObject.GetComponent<Bullet>().damage);

            collision.gameObject.GetComponent<Bullet>().PlaySound();
            Vector3 pos = transform.position;

            pos += Random.insideUnitSphere * 1.15f;
            pos.y += 2.4f;
            TextGenerator.instance.GenerateText(collision.gameObject.GetComponent<Bullet>().damage.ToString(), 5, Color.red, pos);

           

            Destroy(collision.gameObject);
        }
    }

}
