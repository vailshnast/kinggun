﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketEffect : MonoBehaviour
{

    public float moveSpeed;
    public float height = 10;

    private Rigidbody rigidbody;

    private Vector3 targetPos;


    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        targetPos = new Vector3(transform.position.x, height, transform.position.z);


        StartCoroutine(MoveRocket());
    }

    //X(-6,7.5) Y(-4,9)

    private IEnumerator MoveRocket()
    {
        while (Vector3.Distance(transform.position, targetPos) > 0.3)
        {
            Vector3 direction = (targetPos - transform.position).normalized;
            rigidbody.AddForce(direction * moveSpeed);
            //           Debug.Log(Vector3.Distance(transform.position, Player.player.transform.position));
            yield return null;
        }

        Destroy(gameObject);
    }


}
