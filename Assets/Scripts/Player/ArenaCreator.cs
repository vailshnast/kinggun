﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ArenaCreator : MonoBehaviour
{
    public GameObject corner;
    public GameObject wall;
    public List<GameObject> floorTiles;

    [Range(1,50)]
    public int arenaSizeX;
    [Range(1, 50)]
    public int arenaSizeY;

    public bool spawnBoxes;

    public bool showAreaInScene;
    public bool spawnTeleport = true;
    public bool buildNavmesh;
    private float cornerOffset;
    private float tileOffset;

    

    public GameObject teleportPrefab;
    public GameObject teleportPrefabRed;

    public NavMeshSurface surface;

    // Use this for initialization
    void Awake ()
    {
        /*cornerOffset = 2.2606f;
        tileOffset = 1.4732f;*/
        cornerOffset = 2.286f;

        tileOffset = 1.524f;
        //CreateArena(arenaSizeX,arenaSizeY);
        if(spawnTeleport)
        GenerateTeleport();

    }

    [ContextMenu("Create")]
    private void CreateInEditor()
    {
        cornerOffset = 2.286f;

        tileOffset = 1.524f;
        CreateArena(arenaSizeX, arenaSizeY);
    }



  /*  private void CreateArena(int x, int y)
    {
        Vector3 startingPos = transform.position;

        Vector3 pos = Vector3.zero;

        Instantiate(corner, startingPos, Quaternion.Euler(-90, 0, 0),transform);
        for (int i = 0; i < x; i++)
        {
            Instantiate(wall, new Vector3(startingPos.x  - cornerOffset - tileOffset * i, startingPos.y, startingPos.z), Quaternion.Euler(-90,0,0),transform);         
        }

        pos = new Vector3(startingPos.x - cornerOffset * 2 - tileOffset * (x-1), startingPos.y, startingPos.z);

        Instantiate(corner, new Vector3(pos.x,startingPos.y, startingPos.z), Quaternion.Euler(-90, 0, 90),transform);
        
        for (int i = 0; i < y; i++)
        {
            Instantiate(wall, new Vector3(pos.x, startingPos.y , startingPos.z + cornerOffset + tileOffset * i), Quaternion.Euler(-90, 90, 0), transform);
        }

        pos = new Vector3(pos.x, startingPos.y, startingPos.z + cornerOffset * 2 + tileOffset * (y-1));

        Instantiate(corner, new Vector3(pos.x, startingPos.y, pos.z), Quaternion.Euler(-90, 0, -180), transform);

        for (int i = 0; i < x; i++)
        {
            Instantiate(wall, new Vector3(pos.x + cornerOffset + tileOffset * i, startingPos.y, pos.z), Quaternion.Euler(-90, 0, 180), transform);         
        }

        pos = new Vector3(pos.x + cornerOffset * 2 + tileOffset * (x-1), startingPos.y, pos.z);
        Instantiate(corner, new Vector3(pos.x, startingPos.y, pos.z), Quaternion.Euler(-90, 0, -90), transform);

        for (int i = 0; i < y; i++)
        {
            Instantiate(wall, new Vector3(pos.x, startingPos.y, pos.z - cornerOffset - tileOffset * i), Quaternion.Euler(-90, 0, -90), transform);
        }

        startingPos = new Vector3(startingPos.x - cornerOffset, startingPos.y, startingPos.z + cornerOffset);
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                Instantiate(floor, new Vector3(startingPos.x - i * tileOffset, startingPos.y, startingPos.z + j * tileOffset), Quaternion.Euler(-90, 0, 0), transform);
            }            
        }

    }*/


    private void CreateArena(int x, int y)
    {
        Vector3 startingPos = transform.position;

        Vector3 pos = Vector3.zero;

        Instantiate(corner, startingPos, Quaternion.Euler(0, 0, 0), transform);
        for (int i = 0; i < x; i++)
        {
            Instantiate(wall, new Vector3(startingPos.x - cornerOffset - tileOffset * i, startingPos.y, startingPos.z), Quaternion.Euler(0, 0, 0), transform);
        }

        pos = new Vector3(startingPos.x - cornerOffset * 2 - tileOffset * (x - 1), startingPos.y, startingPos.z);

        Instantiate(corner, new Vector3(pos.x, startingPos.y, startingPos.z), Quaternion.Euler(0, 90, 0), transform);

        for (int i = 0; i < y; i++)
        {
            Instantiate(wall, new Vector3(pos.x, startingPos.y, startingPos.z + cornerOffset + tileOffset * i), Quaternion.Euler(0, 90, 0), transform);
        }

        pos = new Vector3(pos.x, startingPos.y, startingPos.z + cornerOffset * 2 + tileOffset * (y - 1));

        Instantiate(corner, new Vector3(pos.x, startingPos.y, pos.z), Quaternion.Euler(0, 180, 0), transform);

        for (int i = 0; i < x; i++)
        {
            Instantiate(wall, new Vector3(pos.x + cornerOffset + tileOffset * i, startingPos.y, pos.z), Quaternion.Euler(0, 180, 0), transform);
        }

        pos = new Vector3(pos.x + cornerOffset * 2 + tileOffset * (x - 1), startingPos.y, pos.z);
        Instantiate(corner, new Vector3(pos.x, startingPos.y, pos.z), Quaternion.Euler(0, 270, 0), transform);

        for (int i = 0; i < y; i++)
        {
            Instantiate(wall, new Vector3(pos.x, startingPos.y, pos.z - cornerOffset - tileOffset * i), Quaternion.Euler(0, 270, 0), transform);
        }

        startingPos = new Vector3(startingPos.x - cornerOffset, startingPos.y, startingPos.z + cornerOffset);
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                float value = Random.value;

                if (spawnBoxes)
                {
                    if (value < 0.03f)
                        Instantiate(floorTiles[Random.Range(1, floorTiles.Count)], new Vector3(startingPos.x - i * tileOffset, startingPos.y, startingPos.z + j * tileOffset), Quaternion.Euler(0, 0, 0), transform);
                    else
                        Instantiate(floorTiles[0], new Vector3(startingPos.x - i * tileOffset, startingPos.y, startingPos.z + j * tileOffset), Quaternion.Euler(0, 0, 0), transform);
                }
                else
                    Instantiate(floorTiles[0], new Vector3(startingPos.x - i * tileOffset, startingPos.y, startingPos.z + j * tileOffset), Quaternion.Euler(0, 0, 0), transform);

            }
        }
        if(buildNavmesh)
        surface.BuildNavMesh();
    }

    private void GenerateTeleport()
    {
        List<GameObject> floors = new List<GameObject>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == "Floor(Clone)")
                floors.Add(transform.GetChild(i).gameObject);
        }

        GameObject randomedFloor = floors[Random.Range(0, floors.Count)];
        Vector3 spawnPos = randomedFloor.transform.position;

        spawnPos.y = 0.076199f;

        Instantiate(teleportPrefab, spawnPos, Quaternion.Euler(-90,0,0), transform);

        spawnPos.x += Random.Range(1, 2);
        spawnPos.z += Random.Range(1, 2);

        Player.player.SetArenaTeleportPos(spawnPos);


        floors.Remove(randomedFloor);
        randomedFloor = floors[Random.Range(0, floors.Count)];
        spawnPos = randomedFloor.transform.position;

        spawnPos.y = 0.076199f;

       
        Instantiate(teleportPrefabRed, spawnPos, Quaternion.Euler(-90, 0, 0), transform);
    }

}
