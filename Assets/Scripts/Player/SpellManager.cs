﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellManager : MonoBehaviour
{
    public Dictionary<SpellType, Spell> spells;

    public static SpellManager spellManager;

    private Spell currentSpell;

    private bool isOnCooldown;
    [HideInInspector]
    public float cooldown;

    public ShowSkillInfo spellSlotInfo;

    public Image skillFadeFillArea;
    public Image skillImage;

    void Awake()
    {
        spellManager = this;

        spells = new Dictionary<SpellType, Spell>
        {
            {SpellType.Heal, new Heal(5.5f, 30, 50)},
            {SpellType.Shield, new Shield(18, 50)},
            {SpellType.Grenade, new Heal(10, 50, 50)},
        };


        //ChooseSpell(SpellType.Heal);
    }

    public void CastSpell()
    {
        if (currentSpell != null && !isOnCooldown && Player.player.energy >= currentSpell.energyConsume)
        {
            isOnCooldown = true;
            Player.player.energy -= currentSpell.energyConsume;
            currentSpell.Cast();
            StartCoroutine(Cooldown());
        }
    }

    private IEnumerator Cooldown()
    {
        cooldown = currentSpell.cooldown;
        while (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
            skillFadeFillArea.fillAmount = cooldown / currentSpell.cooldown;
            yield return null;
        }
        isOnCooldown = false;
    }

    public void ChooseSpell(SpellType type, Sprite image)
    {
        currentSpell = spells[type];
        spellSlotInfo.SetSkill(type, image);
        skillImage.sprite = image;
        Player.player.StartGame();
    }
}
public class Spell
{
    public float cooldown;
    public int energyConsume;

    public Spell(float cooldown, int energyConsume)
    {
        this.cooldown = cooldown;
        this.energyConsume = energyConsume;
    }

    public virtual void Cast()
    {
        
    }
}

public class Shield : Spell
{
    public Shield(float cooldown, int energyConsume) : base(cooldown, energyConsume)
    {

    }

    public override void Cast()
    {
        Player.player.ActivateShield();
    }
}

public class Heal : Spell
{
    private int healAmount;

    public Heal(float cooldown, int energyConsume, int healAmount) : base(cooldown, energyConsume)
    {
        this.healAmount = healAmount;
    }


    public override void Cast()
    {
        Player.player.HealPlayer(healAmount + ((float)healAmount / 100 * PlayerStats.instance.GetStatAmount(PropertyType.SkillPower)));
    }

}








