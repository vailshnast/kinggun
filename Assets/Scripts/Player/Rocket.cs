﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{

    public float moveSpeed;
    public float height = 10;

    private Vector3 startPos;

    private Rigidbody rigidbody;

    public float radius = 10;

    private Vector3 targetPos;
    private GameObject targetPlane;
    public GameObject targetPlanePrefab;
    public GameObject explosion;
    public int damage = 10;

    public AudioClip clip;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        //transform.position = new Vector3(Random.Range(-6, 7.5f), height, Random.Range(-4, 9));

        targetPos = new Vector3(transform.position.x, 0.087f, transform.position.z);
        targetPlane = Instantiate(targetPlanePrefab, targetPos, Quaternion.Euler(90,0,0));

        StartCoroutine(MoveRocket());
    }

    //X(-156,-131) Y(-44,-20)

    private IEnumerator MoveRocket()
    {
        while (Vector3.Distance(transform.position, targetPos) > 0.3)
        {
            Vector3 direction = (targetPos - transform.position).normalized;
            rigidbody.AddForce(direction * moveSpeed);
//           Debug.Log(Vector3.Distance(transform.position, Player.player.transform.position));
            yield return null;
        }

        if(Vector3.Distance(transform.position,Player.player.transform.position) < radius)
            Player.player.DealDamageToPlayer(damage);
        //Debug.Log(Vector3.Distance(Player.player.transform.position,transform.position));
        GameObject effect = Instantiate(explosion, transform.position, Quaternion.Euler(-90,0,0));

        AudioSource.PlayClipAtPoint(clip, Player.player.transform.position, 0.2f);

        Destroy(targetPlane);
        Destroy(effect,1f);
        Destroy(transform.GetChild(0).gameObject);
        Destroy(gameObject,1f);
    }


}
