﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{

    private int health;
    private int maxHealth;
        
    public float runSpeed = 5;

    public float attackDistance = 5;

    private int damage;

    private Rigidbody rigidbody;

    private Animator animatorController;

    private Vector3 randomPoint;

    public int experience = 25;

    private int burstCount;
    private float fireRate;
    private float burstDelay;


    [Header("Refereces")]
    public AudioClip clip;

    public AudioClip fireClip;
    public List<AudioClip> hitClips;

    public CameraFacingBillboard healthSlider;
    public GameObject bullet;
    public Transform bulletPos;
    public GameObject muzzleFlash;
    public GameObject hitEffect;
    private bool showEffect;
    private NavMeshAgent agent;
    
    private bool shooting;

    private bool dead;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        health = 220 + 25 * Player.player.level;
        maxHealth = health;
        burstCount = 10;
        fireRate = 0.2f;
        burstDelay = 1.5f - 0.2f * Player.player.level;
        damage = 25 + 2 * Player.player.level;

        fireRate = Mathf.Clamp(fireRate, 0.05f, 1);
        burstDelay = Mathf.Clamp(burstCount, 0.5f, 1);

        rigidbody = GetComponent<Rigidbody>();
        animatorController = GetComponent<Animator>();

        animatorController.SetFloat("Forward", 1);

        RandomPoint();

    }
    void Update()
    {
        if (DistanceToPlayer() < attackDistance)
        {
            transform.LookAt(Player.player.transform);

          
            if (!shooting)
                StartCoroutine(Shoot());
        }

        else
        {
            StopAllCoroutines();
            shooting = false;

            if(agent.path.corners.Length > 1)
            transform.LookAt(agent.path.corners[1]);
        }

        if(DistanceToPoint() < 2)
            RandomPoint();

        PlayerMovement();

        if (showEffect)
        {
            muzzleFlash.SetActive(true);

            muzzleFlash.transform.localScale = Vector3.one * Random.Range(0.5f, 1.5f);
            muzzleFlash.transform.localEulerAngles = new Vector3(0, 90, Random.Range(0, 360));
        }
        else
        {
            muzzleFlash.SetActive(false);
        }

        //rigidbody.velocity = (randomPoint - transform.position).normalized * runSpeed;
        //transform.position += new Vector3(Mathf.Sin(Time.deltaTime), 0, Mathf.Cos(Time.deltaTime));
    }
    private void RandomPoint()
    {
        randomPoint = new Vector3(Random.Range(-70, 0), 0, Random.Range(0, 70));

        NavMeshHit hit;
        NavMesh.SamplePosition(randomPoint, out hit, 2, 1);

        agent.SetDestination(hit.position);
    }
    private void PlayerMovement()
    {

        float horizontal;
        float vertical;

        if (agent.path.corners.Length > 1)
        {
            horizontal = (agent.path.corners[1] - transform.position).normalized.x;
            vertical = (agent.path.corners[1] - transform.position).normalized.z;
        }
        else
        {
            horizontal = (agent.path.corners[0] - transform.position).normalized.x;
            vertical = (agent.path.corners[0] - transform.position).normalized.z;
        }
        Vector3 move;


        if (Camera.main != null)
        {
            Vector3 camForward = Vector3.Scale(Camera.main.transform.up, new Vector3(1, 0, 1)).normalized;
            move = vertical * camForward + horizontal * Camera.main.transform.right;
        }
        else
        {
            move = vertical * Vector3.forward + horizontal * Vector3.right;
        }

        Vector3 moveInput = move;

        Vector3 localMove = transform.InverseTransformDirection(moveInput);

        float turnAmount = localMove.x;

        float forwardAmount = localMove.z;

        animatorController.SetFloat("Forward", forwardAmount);
        animatorController.SetFloat("Turn", turnAmount);



    }
    private IEnumerator Shoot()
    {
        shooting = true;

        showEffect = true;
        for (int i = 0; i < burstCount; i++)
        {
            Vector3 rotation = bulletPos.transform.parent.rotation.eulerAngles;

            float spread = 20;

            rotation = new Vector3(0, rotation.y + Random.Range(-spread, spread), 0);

            GameObject bulletReference = Instantiate(bullet, bulletPos.transform.position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));

            AudioSource.PlayClipAtPoint(fireClip, transform.position, 0.5f);

            bulletReference.GetComponent<EnemyBullet>().damage = damage;

            yield return new WaitForSeconds(fireRate);
        }
        showEffect = false;
        yield return new WaitForSeconds(burstDelay);

        shooting = false;
    }
    private float DistanceToPoint()
    {
        return Vector3.Distance(transform.position, randomPoint);
    }
    private float DistanceToPlayer()
    {
        return Vector3.Distance(transform.position, Player.player.transform.position);
    }
    private void DealDamage()
    {
        if(dead)
            return;

        if (health - (int) PlayerStats.instance.GetStatAmount(PropertyType.Damage) <= 0)
        {
           // AudioSource.PlayClipAtPoint(clip, Player.player.transform.position, 1);
            Vector3 pos = transform.position;

            pos.y += 1.7f;

            pos += Random.insideUnitSphere * 1.15f;

            TextGenerator.instance.GenerateText(((int)PlayerStats.instance.GetStatAmount(PropertyType.Damage)).ToString(), 4, Color.red, pos);

            dead = true;
            Debug.Log(health);
            Player.player.AddExperience(25 + 10 * Player.player.level);
            Debug.Log("addexp");
            Inventory.instance.AddCredit(200 + 50 * Player.player.level);

            float value = Random.value;

            if (value < 0.2f)
                EnemySpawner.instance.AddLootBox(transform.position);

            EnemySpawner.instance.SpawnEnemy();
            GetComponent<MapMarker>().RemoveMarker();
            Destroy(transform.parent.gameObject);
        }
        else
        {
            if (!healthSlider.isActivated)
                healthSlider.Activate();

            health -= (int) PlayerStats.instance.GetStatAmount(PropertyType.Damage);

            Vector3 pos = transform.position;

            pos += Random.insideUnitSphere * 1.15f;

            pos.y += 1.7f;

            TextGenerator.instance.GenerateText(((int)PlayerStats.instance.GetStatAmount(PropertyType.Damage)).ToString(), 4,Color.red,pos);


            Debug.Log((float)health / maxHealth);
            healthSlider.GetComponent<Slider>().value = (float)health / maxHealth;
        }
    }
    void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.tag == "Bullets")
        {         
            GameObject effect = Instantiate(hitEffect, new Vector3(transform.position.x, 1, transform.position.z), Quaternion.identity);
            Destroy(effect, 0.5f);
            Destroy(collider.gameObject);

            collider.gameObject.GetComponent<Bullet>().PlaySound();

            DealDamage();
        }
        
    }
    public Vector3 WorldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        //Convert the local point to world point
        return parentCanvas.transform.TransformPoint(movePos);
    }
}


