﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bullet : MonoBehaviour
{
    public float lifeTime = 3;

    public float speed = 15;

    public int damage;
   
    private Rigidbody rigidbody;

    private Vector3 velocity;
    public GameObject hitEffect;

    public List<AudioClip> hitClips;


    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    
        
        velocity = Player.player.rigidbody.velocity;
    }

    void Start()
    {
        StartCoroutine(Move());
    }

    private IEnumerator Move()
    {
       
        while (lifeTime > 0)
        {
            lifeTime -= Time.deltaTime;

            rigidbody.velocity = /*velocity +*/ transform.forward * speed;


            yield return null;
        }
        Destroy(gameObject);
    }

    public void PlaySound()
    {
        AudioClip clip = hitClips[Random.Range(0, hitClips.Count)];
        AudioSource.PlayClipAtPoint(clip, Player.player.transform.position + Random.insideUnitSphere * 2, 0.2f);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall" && collision.gameObject.name != "Arch")
        {   
            //PlaySound();         
            //GameObject effect = Instantiate(hitEffect,  -transform.forward.normalized * 1.2f + transform.position, Quaternion.identity);
            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.Euler(-180,0,0));
            Destroy(effect, 0.5f);
            Destroy(gameObject);
        }
    }
}
