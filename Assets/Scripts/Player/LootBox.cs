﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBox : MonoBehaviour
{
    private ItemInfo itemInfo;

    public float rotateSpeed;

    public List<ItemInfoTemplate> templates;

    public float startOffset = 0.5f;
    public float maxOffset = 0.6f;

    public float pingPongSpeed = 0.65f;

    private float y;

    private float rot;

    public void RandomItem(int level)
    {
        itemInfo = new ItemInfo(templates[Random.Range(0, templates.Count)], level);
    }

    public void SetItem(ItemInfo item)
    {
        itemInfo = new ItemInfo(item);
    }


    void Update()
    {
        rot += Time.deltaTime * rotateSpeed;
        transform.rotation = Quaternion.Euler(-90, 0, rot);
        y = Mathf.PingPong(Time.time * pingPongSpeed, maxOffset);
        transform.position = new Vector3(transform.position.x,y + startOffset,transform.position.z);
    }
    

   /* void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            if (Inventory.instance.AddItemToInventory(itemInfo))
            {
                //EnemySpawner.instance.SpawnLootWithDelay();
                Destroy(gameObject);
            }
        }
    }*/

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "Player")
        {
            if (Inventory.instance.AddItemToInventory(itemInfo))
            {
                //EnemySpawner.instance.SpawnLootWithDelay();
                Destroy(gameObject);
            }
        }
    }

}
