﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{

    public float lifeTime = 3;

    public float speed = 15;

    private Rigidbody rigidbody;

    private Vector3 velocity;

    public int damage = 10;

    public GameObject hitEffect;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        Destroy(gameObject,lifeTime);
    }

    void Update()
    {
        rigidbody.AddForce(transform.forward * speed);
    }


    void OnTriggerEnter(Collider coll)
    {
        if (coll.name == "Player")
        {
            Player.player.DealDamageToPlayer(damage);
            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(effect, 0.5f);
            Destroy(gameObject);
        }

        if (coll.tag == "Wall")
        {
            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(effect, 0.5f);
            Destroy(gameObject);
        }
                    
    }

}
