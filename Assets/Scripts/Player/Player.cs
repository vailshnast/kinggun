﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class Player : MonoBehaviour
{
    [Header("Variables")]
    private float runSpeed = 3;
    [HideInInspector]
    public float health = 20;
    [HideInInspector]
    public float energy;

    private float currentHeat;
    private int currentAmmo;

    private WeaponSlot offslot;
    public ShootState shootState;

    private Vector3 arenaTeleportPos;

    [HideInInspector]
    public int level;
    private int experience;

    private float shootCooldown;
    private float cooldownTime;
    private bool canShoot;


    public PlayerInventoryData data;

    [Header("Camera")]

    #region Camera

    public float moveCameraStrength = 1;
    public float cameraSmooth = 0.01f;
    public float maxDistance = 10f;
    public float cursorPlaneHeight = 0;
    public float cursorHeight = 0;
    public float cameraRotateSpeed = 1;

        #endregion

    [Header("Refereces")]

    #region References

    public AudioClip swapSound;
    public AudioClip reloadSound;
    public Image firstSlot;

    public Image secondSlot;
    public GameObject levelUpEffectPrefab;
    public GameObject shield;
    public GameObject bullet;
    public Transform bulletPos;
    public GameObject muzzleFlash;
    public GameObject inventoryPanel;
    public GameObject skillPanel;
    public GameObject stats;
    public TextMeshProUGUI levelText;
    public static Player player;
    public GameObject healEffect;

    public GameObject lootBox;

    private Transform camera;
    private Animator animatorController;
    [HideInInspector]
    public Rigidbody rigidbody;

    public RectTransform healthBar;
    public RectTransform manaBar;
    public RectTransform expBar;
    public RectTransform heatBar;

    private AudioSource audioSource;

    public GameObject experienceTextPrefab;

    public ParticleSystem dashEffect;

    #endregion

    #region Dash

    private Vector3 dashDirection;
    private float dashPower;

    

    private bool isDashCoroutineOnCooldown;

    private int currentAvailableDashCharges;
    private float dashChargeCooldown;

    #endregion

    public enum ShootState
    {
        Shooting,
        NotShooting,
        Reload
    }

    public enum PlayerLocation
    {
        Home,
        Battle
    }

    private PlayerLocation playerLocation;
    

    #region CameraAndAnimations

    private DebugAnimationStates animationState;
    private AnimatorStateInfo animationInfo;
    private Vector3 offsetFromCamToPlayer;
    private Plane playerMovementPlane;
    private Quaternion screenMovementSpace ;
    private Vector3 screenMovementForward;
    private Vector3 screenMovementRight;

    #endregion
    #region Initialization

    void Awake()
    {
        player = this;
        #region References

        camera = Camera.main.transform;
        animatorController = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
        animationState = new DebugAnimationStates();
        audioSource = GetComponent<AudioSource>();

        #endregion
    }

    void Start()
    {
        SetVariables();
    }


    #endregion

    #region SetFuntions

    private void SetVariables()
    {
      

        #region Stats

        health = (int) PlayerStats.instance.GetStatAmount(PropertyType.Health);
        shootCooldown = 1 / (PlayerStats.instance.GetStatAmount(PropertyType.FireRate) / 60);



        energy = (int) PlayerStats.instance.GetStatAmount(PropertyType.Energy);

        runSpeed = PlayerStats.instance.GetStatAmount(PropertyType.Speed);

        #endregion

        offslot = new WeaponSlot(firstSlot,secondSlot);

        currentAvailableDashCharges = 1;

        shootState = ShootState.NotShooting;

        canShoot = true;

        playerLocation = PlayerLocation.Home;

        #region Camera

        playerMovementPlane = new Plane(transform.up, transform.position + transform.up * cursorPlaneHeight);
        offsetFromCamToPlayer = camera.position - transform.position;

        screenMovementSpace = Quaternion.Euler(0, camera.eulerAngles.y, 0);
        screenMovementForward = screenMovementSpace * Vector3.forward;
        screenMovementRight = screenMovementSpace * Vector3.right;

        #endregion

        StartCoroutine(RegenerateHealthAndEnergy());

        level = 1;
        experience = 0;

        Time.timeScale = 0;

        Cursor.visible = true;
    }

    #endregion

    

    // Update is called once per frame
    void Update()
    {       
        UpdateUIAndStats();
        PlayerInput();
        DashChargesReplenish();
    }

 

    void FixedUpdate()
    {
        if (!Shop.instance.isOpened)
        {          
            CameraFollowPlayer();
            PlayerMovement();
            PlayerLookAtCursor();        
        }

    }

    #region Stats

    private void ToggleInventory()
    {
        Inventory.instance.ToggleInventory();

    }
    private void ToggleStats()
    {
        Inventory.instance.ToggleStats();
    }
    public void DealDamageToPlayer(int amount, bool pierceArmour = false)
    {
        int damage = amount;

        if (pierceArmour == false)
            damage = Mathf.Clamp(amount - (int)PlayerStats.instance.GetStatAmount(PropertyType.Armor), (int)(amount * 0.3f), amount);

        health -= damage;

        Vector3 pos = transform.position;

        //pos += Random.insideUnitSphere * 1.3f;

        pos.y += 1.7f;

        TextGenerator.instance.GenerateText(damage.ToString(), 4, Color.red, pos);

        if (health <= 0)
        {
            int deathPrice = 0;/*500 + level * 1000;*/

            health = PlayerStats.instance.GetStatAmount(PropertyType.Health);

            if (Inventory.instance.data.credits >= deathPrice)
            {
                Inventory.instance.AddCredit(-deathPrice);
                Spider.spider.Reset();
                TeleportHome();
            }
            else
            {
                SceneManager.LoadScene(0);
            }


            //data.playerIsDead = true;
            //SceneManager.LoadScene(0);
        }

    }

    public void AddExperience(int amount)
    {
        int totalAmount = amount + experience;

        int levelThreshhold = level * 400;

        if (totalAmount >= levelThreshhold)
        {
            experience = totalAmount - levelThreshhold;
            level++;

            GameObject effectUp = Instantiate(levelUpEffectPrefab, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
            Destroy(effectUp, 0.5f);
            if (level % 2 == 0)
            {
                
                EnemySpawner.instance.ConstructEnemy();
            }
        }
        else experience += amount;

        //CreateExpText(amount);

        expBar.GetComponent<Slider>().value = (float)experience / levelThreshhold;

        levelText.text = "Level : " + level;

        //Update Ui
    }
    private void UpdateUIAndStats()
    {
        //text.text = "Speed : " + runSpeed + "\nSpider health: " + Spider.spider.health;
        healthBar.GetComponent<Slider>().value = (float)health / (int)PlayerStats.instance.GetStatAmount(PropertyType.Health);
        manaBar.GetComponent<Slider>().value = (float)energy / (int)PlayerStats.instance.GetStatAmount(PropertyType.Energy);

        if (shootState != ShootState.Reload)
            heatBar.GetComponent<Slider>().value = currentAmmo / PlayerStats.instance.GetStatAmount(PropertyType.Ammo);

        /*chargesText.text = "Dash Charges: " + currentAvailableDashCharges + " / " + 2 + "\n" +
                           "Dash Cooldown: " + string.Format("{0:0.0}", dashChargeCooldown) + " / " + string.Format("{0:0.0}", 2.5f) + "\n" +
                           "Weapon Cooldown: " + string.Format("{0:0.0}", cooldownTime) +"\n" +
                           "Skill Cooldown: " + string.Format("{0:0.0}", SpellManager.spellManager.cooldown);*/



        healthBar.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = (int)health + " / " + (int)PlayerStats.instance.GetStatAmount(PropertyType.Health);
        manaBar.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = (int)energy + " / " + (int)PlayerStats.instance.GetStatAmount(PropertyType.Energy);
        heatBar.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Ammo: " + (int)currentAmmo;/*+ "/ " + (int)PlayerStats.instance.GetStatAmount(PropertyType.Ammo);*/

        runSpeed = PlayerStats.instance.GetStatAmount(PropertyType.Speed);
    }

    #endregion

    #region Skills
    #region Dash

    private void Dash()
    {
        if (rigidbody.velocity == Vector3.zero)
            return;

        int energyAmount = 25;

        if (!isDashCoroutineOnCooldown && /*&& currentAvailableDashCharges > 0*/  energy >= energyAmount)
        {
            currentAvailableDashCharges -= 1;

            energy -= energyAmount;
            StartCoroutine(DashCoroutine());
        }

    }
    private IEnumerator DashCoroutine()
    {
        isDashCoroutineOnCooldown = true;

        //currentHeat = Mathf.Clamp(currentHeat - PlayerStats.instance.GetStatAmount(PropertyType.HeatPool) / 100 * 15 /*DASH COOLING = 15 NOW*/, 0, 200);

        dashEffect.Play();
        dashEffect.Clear();

        dashDirection = rigidbody.velocity.normalized;

        dashPower = 15f;

        float time = 0.45f;

        while (time > 0)
        {
            //dashPower -= Time.deltaTime / seconds;
            dashPower = Mathf.Lerp(dashPower, 0, Time.deltaTime);

            time -= Time.deltaTime;

            yield return null;
        }
        dashPower = 0;
        dashEffect.Stop();

        //Basic cooldown
        yield return new WaitForSeconds(0.3f);

        isDashCoroutineOnCooldown = false;
    }
    private void DashChargesReplenish()
    {
        if (currentAvailableDashCharges < 2)
        {
            dashChargeCooldown += Time.deltaTime;
            if (dashChargeCooldown >= 2.5f)
            {
                dashChargeCooldown = 0;
                currentAvailableDashCharges++;
            }
        }
        else if (currentAvailableDashCharges > 2)
            currentAvailableDashCharges = 2;
    }

    #endregion
    public void HealPlayer(float healAmount, bool percentage = false)
    {
        float maxHealth = PlayerStats.instance.GetStatAmount(PropertyType.Health);

        float totalHealth = percentage ? health + maxHealth / 100 * healAmount : health + healAmount;

        health = totalHealth >= maxHealth ? maxHealth : totalHealth;

        Vector3 pos = transform.position;
        pos.y = 1;

        CreateEffect(healEffect, pos, 0.7f);
    }
    #region Shield

    public void ActivateShield()
    {
        StartCoroutine(ActivateShieldCorotutine());
    }

    private IEnumerator ActivateShieldCorotutine()
    {
        shield.SetActive(true);
        yield return new WaitForSeconds(3 + (float)3 / 100 * PlayerStats.instance.GetStatAmount(PropertyType.SkillPower));
        shield.SetActive(false);
    }

    #endregion
    #endregion

    #region Actions
    /* private void OverHeat()
 {
     StartCoroutine(OverHeatCoroutine());
 }


 private IEnumerator OverHeatCoroutine()
 {
     shootState = ShootState.Reload;

     muzzleFlash.SetActive(false);
     currentHeat = 0;
     float totalOverHeatTime = 3;

     DealDamageToPlayer((int) (PlayerStats.instance.GetStatAmount(PropertyType.Health) * 0.3f));

     float time = totalOverHeatTime;

     while (time > 0)
     {
         time -= Time.deltaTime;

         heatBar.GetComponent<Slider>().value = time / totalOverHeatTime;

         yield return null;
     }
     //ammo = (int)PlayerStats.instance.GetStatAmount(PropertyType.Ammo);

     shootState = ShootState.NotShooting;
 }*/
    #region Shoot

    private IEnumerator ShootCooldown()
    {
        float animationTime = 0.15f;
        cooldownTime = 0.2f;

        switch (Inventory.instance.GetItem(ItemType.Weapon).weaponType)
        {
            case WeaponType.Rifle:
                
                break;
            case WeaponType.ShotGun:
                cooldownTime = 0.5f;
                break;
            case WeaponType.Pistol:
                //animationTime = 0.1f;
                cooldownTime = 0.1f;
                break;
            case WeaponType.SniperRifle:
                cooldownTime = 0.8f;
                break;
        }

        muzzleFlash.SetActive(true);

        canShoot = false;
        
        while (cooldownTime > 0)
        {


            animationTime -= Time.deltaTime;
            if (animationTime > 0)
            {

                muzzleFlash.transform.localScale = Vector3.one * Random.Range(0.5f, 1.5f);
                muzzleFlash.transform.localEulerAngles = new Vector3(0, 90, Random.Range(0, 360));
            }
            else muzzleFlash.SetActive(false);


            cooldownTime -= Time.deltaTime;
            yield return null;
        }
        cooldownTime = 0;
        muzzleFlash.SetActive(false);
        canShoot = true;
    }
    private void Shoot()
    {
        muzzleFlash.SetActive(true);

        switch (Inventory.instance.GetItem(ItemType.Weapon).weaponType)
        {
            case WeaponType.Rifle:
                audioSource.volume = 1;
                break;
            case WeaponType.ShotGun:
                audioSource.volume = 0.5f;
                break;
            case WeaponType.Pistol:
                audioSource.volume = 0.7f;
                break;
            case WeaponType.SniperRifle:
                audioSource.volume = 1;
                break;
        }

        audioSource.PlayOneShot(Inventory.instance.GetItem(ItemType.Weapon).weaponSound);

        

        muzzleFlash.transform.localScale = Vector3.one * Random.Range(0.5f, 1.5f);
        muzzleFlash.transform.localEulerAngles = new Vector3(0, 90, Random.Range(0, 360));

        for (int i = 0; i < PlayerStats.instance.GetStatAmount(PropertyType.BulletCount); i++)
        {
            Vector3 rotation = bulletPos.transform.parent.rotation.eulerAngles;

            float spread = PlayerStats.instance.GetStatAmount(PropertyType.Spread) / 2;

            rotation = new Vector3(0, rotation.y + Random.Range(-spread, spread), 0);

            GameObject bulletReference = Instantiate(bullet, bulletPos.transform.position, Quaternion.Euler(rotation.x, rotation.y, rotation.z));
            bulletReference.GetComponent<Bullet>().damage = (int)PlayerStats.instance.GetStatAmount(PropertyType.Damage);
            bulletReference.GetComponent<Bullet>().lifeTime = PlayerStats.instance.GetStatAmount(PropertyType.Range);
        }

        currentAmmo--;

        if (currentAmmo <= 0)
            Reload();
    }
    public void Reload()
    {

        if (currentAmmo != (int) PlayerStats.instance.GetStatAmount(PropertyType.Ammo) && shootState != ShootState.Reload)
        {
            StartCoroutine("ReloadCoroutine");
        }
            
    }
    private void ProcessShooting()
    {
        switch (shootState)
        {
            case ShootState.Shooting:
                muzzleFlash.transform.localScale = Vector3.one * Random.Range(0.5f, 1.5f);
                muzzleFlash.transform.localEulerAngles = new Vector3(0, 90, Random.Range(0, 360));
                if (shootCooldown > 0)
                {
                    shootCooldown -= Time.deltaTime;
                }
                else
                {
                    Shoot();
                    shootCooldown = 1 / (PlayerStats.instance.GetStatAmount(PropertyType.FireRate) / 60);
                }

                break;
            case ShootState.NotShooting:
                muzzleFlash.SetActive(false);

                /*float coolingAmount = 0;

                if (Inventory.instance.GetItem(ItemType.Weapon) != null)
                    coolingAmount += Inventory.instance.GetItem(ItemType.Weapon).GetStatAmount(PropertyType.Cooling).propertyAmount;

                if (currentHeat > 0)
                    currentHeat -= Time.deltaTime / (1 / coolingAmount);
                else
                    currentHeat = 0;*/
                break;
            case ShootState.Reload:

                break;
        }
    }
    private IEnumerator ReloadCoroutine()
    {
        shootState = ShootState.Reload;
        
        muzzleFlash.SetActive(false);

        currentAmmo = 0;

        float reloadTime = 0;

        while (reloadTime < PlayerStats.instance.GetStatAmount(PropertyType.ReloadTime))
        {
            reloadTime += Time.deltaTime;

            heatBar.GetComponent<Slider>().value = reloadTime / PlayerStats.instance.GetStatAmount(PropertyType.ReloadTime);

            yield return null;
        }
        AudioSource.PlayClipAtPoint(reloadSound,transform.position,1);
        currentAmmo = (int)PlayerStats.instance.GetStatAmount(PropertyType.Ammo);
        shootState = ShootState.NotShooting;
    }

    public void StopReloading()
    {
        StopCoroutine("ReloadCoroutine");
        shootState = ShootState.NotShooting;
    }

    #endregion
    private void TeleportHome()
    {
        AudioManager.audioManager.ChangeMusicVolume(0.1f);

        EnemySpawner.instance.StopEnemySpawn();

        transform.position = new Vector3(-70, 0, -70);


        Vector3 cameraPos = transform.position + offsetFromCamToPlayer + GetCameraAdjustmentVector() * moveCameraStrength;

        camera.position = cameraPos;
        playerLocation = PlayerLocation.Home;
    }
    public void StartGame()
    {
        Time.timeScale = 1;
        skillPanel.SetActive(false);
    }

    #endregion

    #region Physics
    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "TeleportToArena")
        {
            AudioManager.audioManager.ChangeMusicVolume(0.4f);

            transform.position = arenaTeleportPos;

            Vector3 cameraPos = transform.position + offsetFromCamToPlayer + GetCameraAdjustmentVector() * moveCameraStrength;

            camera.position = cameraPos;
            EnemySpawner.instance.StartEnemySpawn();
            playerLocation = PlayerLocation.Battle;
        }

        if (coll.tag == "TeleportToBoss")
        {
            AudioManager.audioManager.ChangeMusicVolume(0.4f);
            transform.position = new Vector3(-156.15f, transform.position.y, -44f);

            Vector3 cameraPos = transform.position + offsetFromCamToPlayer + GetCameraAdjustmentVector() * moveCameraStrength;

            camera.position = cameraPos;

            Spider.spider.ChangePhase(Spider.Phase.BulletHell, 1);

            //playerLocation = PlayerLocation.Battle;
        }

        if (coll.tag == "TeleportToHomeFromBoss")
        {
            TeleportHome();
            Spider.spider.Reset();
        }

        if (coll.tag == "TeleportToHome")
        {
            TeleportHome();
        }

        if (coll.name == "Shop")
        {
            Shop.instance.ShowActivationTooltip();
        }
    }
    void OnTriggerExit(Collider coll)
    {
        if (coll.name == "Shop")
        {
            Shop.instance.canBeOpened = false;
            Shop.instance.HideActivationTooltip();
        }
    }
    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Wall")
            dashPower = 0;
    }

    #endregion

    #region InputAndMovement

    private void PlayerInput()
    {
        if (!Shop.instance.isOpened)
        {
            if(PlayerStats.instance.GetStatAmount(PropertyType.BulletCount) > 0 && !IsPointerOverUIObject() && shootState != ShootState.Reload)
            {
                switch (Inventory.instance.GetItem(ItemType.Weapon).fireType)
                {
                    case FireType.Semi:

                        if (Input.GetMouseButtonDown(0))
                        {
                            if (canShoot)
                            {
                                Shoot();

                                muzzleFlash.transform.localScale = Vector3.one * Random.Range(0.5f, 1.5f);
                                muzzleFlash.transform.localEulerAngles = new Vector3(0, 90, Random.Range(0, 360));

                                StartCoroutine(ShootCooldown());
                            }
                        }
                        break;
                    case FireType.Auto:
                                  
                        if (Input.GetMouseButton(0))
                        {
                            if (shootState == ShootState.NotShooting)
                            {
                                shootCooldown = 0;
                                shootState = ShootState.Shooting;
                            }
                        }
                        else/* if (shootState != ShootState.OverHeat)*/
                            shootState = ShootState.NotShooting;

                        ProcessShooting();
                        break;
                }
            }

        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Dash();
        }

        if (Input.GetKey(KeyCode.E))
        {
            if (Shop.instance.canBeOpened)
                Shop.instance.OpenShop();
        }

        if (Input.GetKey(KeyCode.R))
        {
            Reload();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleInventory();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            ToggleStats();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SpellManager.spellManager.CastSpell();
        }

        if(Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            AudioSource.PlayClipAtPoint(swapSound, transform.position, 1f);
            offslot.SwapSlot(ref currentAmmo);
        }

              
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Shop.instance.isOpened)
            {
                Shop.instance.canBeOpened = true;
                Shop.instance.CloseShop();
            }

            
        }                   
    }
    private void PlayerMovement()
    {
        if (GetCurrentState() != "Locomotion")
            return;

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontal, 0, vertical);

        movement.Normalize();

        Vector3 move;


        if (camera != null)
        {
            Vector3 camForward = Vector3.Scale(camera.up, new Vector3(1, 0, 1)).normalized;
            move = vertical * camForward + horizontal * camera.right;
        }
        else
        {
            move = vertical * Vector3.forward + horizontal * Vector3.right;
        }

        Vector3 moveInput = move;

        Vector3 localMove = transform.InverseTransformDirection(moveInput);

        float turnAmount = localMove.x;

        float forwardAmount = localMove.z;



        rigidbody.velocity = movement * runSpeed + dashDirection * dashPower;
        animatorController.SetFloat("Forward", forwardAmount);
        animatorController.SetFloat("Turn", turnAmount);



    }
    #endregion

    #region Other
    public void SetArenaTeleportPos(Vector3 spawnPos)
    {
        arenaTeleportPos = spawnPos;
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
    private IEnumerator RegenerateHealthAndEnergy()
    {
        while (true)
        {
        

            int mult = playerLocation == PlayerLocation.Home ? 3 : 0;

            //Debug.Log(mult);

            float totalHealth = health + PlayerStats.instance.GetStatAmount(PropertyType.HealthRegen) / 100 * 2 + mult;

            if (totalHealth >= PlayerStats.instance.GetStatAmount(PropertyType.Health))
                health = PlayerStats.instance.GetStatAmount(PropertyType.Health);

            else health += PlayerStats.instance.GetStatAmount(PropertyType.HealthRegen) / 100 * 2 + mult;

            float totalEnergy = energy + PlayerStats.instance.GetStatAmount(PropertyType.EnergyRegen) / 100 * 2 + mult;

            if (totalEnergy >= PlayerStats.instance.GetStatAmount(PropertyType.Energy))
                energy = PlayerStats.instance.GetStatAmount(PropertyType.Energy);

            else energy += PlayerStats.instance.GetStatAmount(PropertyType.EnergyRegen) / 100 * 2 + mult;

            yield return new WaitForSeconds(0.01f);
        }
    }    
    public string GetCurrentState()
    {
        animationInfo = animatorController.GetCurrentAnimatorStateInfo(0);
        return animationState.Get(animationInfo.tagHash);
    }
    public void CreateEffect(GameObject effectPrefab, Vector3 pos, float deleteTime)
    {
        GameObject effect = Instantiate(effectPrefab, pos, Quaternion.identity);

        Destroy(effect, deleteTime);
    }
    private void CreateExpText(int exp)
    {
        GameObject expRect = Instantiate(experienceTextPrefab, FindObjectOfType<Canvas>().transform).gameObject;
        StartCoroutine(MoveText(expRect, 0.5f, 0.5f, 1.5f, exp));
    }
    private IEnumerator MoveText(GameObject textObject, float live_time,float fade_time, float speed, int exp)
    {
        RectTransform rect = textObject.GetComponent<RectTransform>();
        TextMeshProUGUI text = textObject.GetComponent<TextMeshProUGUI>();

        text.text = "+" + exp;

        rect.anchoredPosition = new Vector2(0, 110);

        float t = 0f;

        while (t <= 1)
        {
            t += Time.deltaTime / live_time;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y + speed);
            yield return null;
        }

        t = 0f;
        text.canvasRenderer.SetAlpha(1.0f);
        text.GetComponent<TextMeshProUGUI>().CrossFadeAlpha(0, fade_time, false);

        while (t <= 1)
        {
            t += Time.deltaTime / fade_time;
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y + speed);
            yield return null;
        }
        Destroy(textObject);
    }
    #endregion

    #region CameraAndCursorControl

    private void CameraFollowPlayer()
    {
        Vector3 mouseDelta = new Vector3(Input.GetAxis("Mouse X"),0, Input.GetAxis("Mouse Y"));

        Vector3 cameraVelocity = Vector3.zero;

        //camera.transform.position  = transform.position + currentOffsetToPlayer + new Vector3(Input.GetAxis("Mouse X") * moveCameraStrength, 0, Input.GetAxis("Mouse Y") * moveCameraStrength);
        Vector3 mousePos = ScreenToWorldPoint();

        //Debug.Log(mousePos - transform.position);

        Vector3 cameraPos = transform.position + offsetFromCamToPlayer + GetCameraAdjustmentVector() * moveCameraStrength;

        //To do check dist yopta
        //Debug.Log(Vector3.Distance(transform.position, camera.position));

        //camera.position = new Vector3(cameraPos.x, camera.position.y, cameraPos.z);

        //camera.position = Vector3.Lerp(camera.position, cameraPos, Time.deltaTime * cameraSmooth);
        Vector3 vel = Vector3.zero;
        camera.position = Vector3.SmoothDamp(camera.position,cameraPos,ref vel,0.1f);
        //camera.position = cameraPos;                                
    }

    private Vector3 GetCameraAdjustmentVector()
    {
        Vector3 cameraAdjustmentVector;

        playerMovementPlane.normal = transform.up;
        playerMovementPlane.distance = -transform.position.y + cursorPlaneHeight;

        Vector3 cursorScreenPosition = Input.mousePosition;/*new Vector3(Input.mousePosition.x,Input.mousePosition.y * 1.5f,0);*/

        // Find out where the mouse ray intersects with the movement plane of the player
        Vector3 cursorWorldPosition = ScreenPointToWorldPointOnPlane(playerMovementPlane, cursorScreenPosition);

        float halfWidth = Screen.width / 2.0f;
        float halfHeight  = Screen.height / 2.0f;
        float maxHalf  = Mathf.Max(halfWidth, halfHeight);

        // Acquire the relative screen position			
        Vector3 posRel  = cursorScreenPosition - new  Vector3(halfWidth, halfHeight, cursorScreenPosition.z);
        posRel.x /= maxHalf;
        posRel.y /= maxHalf;

        cameraAdjustmentVector = posRel.x * screenMovementRight + posRel.y * screenMovementForward;

        cameraAdjustmentVector.z *= (float)Screen.width / Screen.height;

        if (cameraAdjustmentVector.z < 0)
            cameraAdjustmentVector.z *= 1.3f;

        cameraAdjustmentVector.z = Mathf.Clamp(cameraAdjustmentVector.z, -1.2f, 1.2f);
        
        return cameraAdjustmentVector;
    }

    public Vector3 ScreenPointToWorldPointOnPlane(Plane plane,Vector3 screenPoint) {
        // Set up a ray corresponding to the screen position
        Ray ray = Camera.main.ScreenPointToRay (screenPoint);
	
        // Find out where the ray intersects with the plane
        return PlaneRayIntersection(plane, ray);
    }

    

    public Vector3 PlaneRayIntersection(Plane plane, Ray ray)
    {
        float dist;
        plane.Raycast (ray, out dist);
        return ray.GetPoint (dist);
    }

    private void PlayerLookAtCursor()
    {
        if (GetCurrentState() != "Locomotion" )
            return;

        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        ray.origin = new Vector3(ray.origin.x, ray.origin.y - 1.308893f, ray.origin.z);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            // Smoothly rotate towards the target point.
            //transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);

            transform.rotation = targetRotation;
        }
    }

    private Vector3 ScreenToWorldPoint()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            return ray.GetPoint(hitdist);
        }
        return Vector3.zero;
    }

    public Vector3 WorldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        //Convert the local point to world point
        return parentCanvas.transform.TransformPoint(movePos);
    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    #endregion
}
public  class DebugAnimationStates
{

    private  Dictionary<int, string> m_states = new Dictionary<int, string>();

    public DebugAnimationStates()
    {
        Add("Locomotion");
        Add("Walk");
        Add("Horizontal Attack");
        Add("Vertical Attack");
    }

    public string Get(int _fullPathHash)
    {
        string name;
        if (m_states.TryGetValue(_fullPathHash, out name))
            return name;

        return "Unknown#" + _fullPathHash;
    }

    private  void Add(string _stateName)
    {
        int hash = Animator.StringToHash(_stateName);
        m_states.Add(hash, _stateName);
    }
}
public class WeaponSlot
{
    public ItemInfo itemInfo;
    public int ammo;

    private Image weaponFirstSlotImage;
    private Image weaponSecondSlotImage;

    private Image currentSlotImage;


    public WeaponSlot(Image weaponFirstSlotImage, Image weaponSecondSlotImage)
    {
        this.weaponFirstSlotImage = weaponFirstSlotImage;
        this.weaponSecondSlotImage = weaponSecondSlotImage;

        currentSlotImage = weaponFirstSlotImage;

        ammo = 0;
        itemInfo = null;
    }

    public void SwapSlot(ref int ammo)
    {
        int cachedAmmo = ammo;
        ammo = this.ammo;
   
        this.ammo = cachedAmmo;

        if (currentSlotImage == weaponFirstSlotImage)
        {
            currentSlotImage.enabled = false;
            currentSlotImage = weaponSecondSlotImage;
            currentSlotImage.enabled = true;
        }
        else
        {
            currentSlotImage.enabled = false;
            currentSlotImage = weaponFirstSlotImage;
            currentSlotImage.enabled = true;
        }
          
            
      


        ItemDragHandler itemDragHandler = Inventory.instance.GetModuleSlot(ItemType.Weapon).ItemDragHandler;
        ItemInfo cachedItem = null;
        if (itemDragHandler.itemInfo != null)
        {
            itemDragHandler.RemoveItemProperties(false);
            cachedItem = new ItemInfo(itemDragHandler.itemInfo);
        }

        if (itemInfo != null)
        {
            itemDragHandler.SetUpItem(itemInfo);
            itemDragHandler.AddItemProperties(false);
        }          
        else
            itemDragHandler.RemoveItem();

        if (ammo > 0)
        {
            if (Player.player.shootState == Player.ShootState.Reload)
                Player.player.StopReloading();
        }
        else
        {
            if (Player.player.shootState != Player.ShootState.Reload)
            {
                Player.player.Reload();
            }
        }

        itemInfo = cachedItem;
    }

}