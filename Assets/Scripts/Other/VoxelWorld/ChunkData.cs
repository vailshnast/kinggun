﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkData : MonoBehaviour
{
    public int chunkSize;


    public int chunkX;
    public int chunkY;
    public int chunkZ;
    private int faceCount;

    private Mesh mesh;
    private MeshCollider meshCollider;
    private MeshFilter meshFilter;

    [HideInInspector] public List<Vector3> vertices;
    [HideInInspector] public List<int> triangles;
    [HideInInspector] public List<Vector2> uvCoords;

    private float tUnit = 0.0625f;
    private Vector2 stoneUV = new Vector2(1, 15);
    private Vector2 grassUV = new Vector2(3, 15);
    private Vector2 tGrassTop = new Vector2(2, 6);

    private void Awake()
    {
        vertices = new List<Vector3>();
        triangles = new List<int>();
        uvCoords = new List<Vector2>();

        meshFilter = GetComponent<MeshFilter>();
        mesh = meshFilter.mesh;

        meshCollider = GetComponent<MeshCollider>();
        //adjustedScale = scale * 0.5f;


    }

    void Start()
    {
        GenerateMesh();
    }


    public void SetChunkData()
    {
        mesh.Clear();

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uvCoords.ToArray();
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        vertices.Clear();
        triangles.Clear();
        uvCoords.Clear();

        faceCount = 0;
        meshCollider.sharedMesh = null;
        meshCollider.sharedMesh = mesh;
    }

    public int Block(int x, int y, int z)
    {
        return VoxelWorld.World.Block(x + chunkX, y + chunkY, z + chunkZ);
    }

    public void GenerateMesh()
    {
        for (int x = 0; x < chunkSize; x++)
        {
            for (int y = 0; y < chunkSize; y++)
            {
                for (int z = 0; z < chunkSize; z++)
                {
                    //This code will run for every block in the chunk

                    if (Block(x, y, z) != 0)
                    {
                        //If the block is solid

                        if (Block(x, y + 1, z) == 0)
                        {
                            //Block above is air
                            CubeTop(x, y, z, Block(x, y, z));
                        }

                        if (Block(x, y - 1, z) == 0)
                        {
                            //Block below is air
                            CubeBot(x, y, z, Block(x, y, z));

                        }

                        if (Block(x + 1, y, z) == 0)
                        {
                            //Block east is air
                            CubeEast(x, y, z, Block(x, y, z));

                        }

                        if (Block(x - 1, y, z) == 0)
                        {
                            //Block west is air
                            CubeWest(x, y, z, Block(x, y, z));

                        }

                        if (Block(x, y, z + 1) == 0)
                        {
                            //Block north is air
                            CubeNorth(x, y, z, Block(x, y, z));

                        }

                        if (Block(x, y, z - 1) == 0)
                        {
                            //Block south is air
                            CubeSouth(x, y, z, Block(x, y, z));

                        }

                    }
                }
            }
        }

        SetChunkData();
    }

    #region CubeFuntions

    void CubeTop(int x, int y, int z, int block)
    {

        /* vertices.Add(new Vector3(x, y, z + 1));
         vertices.Add(new Vector3(x + 1, y, z + 1));
         vertices.Add(new Vector3(x + 1, y, z));
         vertices.Add(new Vector3(x, y, z));*/

        vertices.Add(new Vector3(x, y, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x, y, z) * VoxelWorld.World.adjustedScale);

        Vector2 texturePos = stoneUV;

        if (Block(x, y, z) == 1)
        {
            texturePos = stoneUV;
        }
        else if (Block(x, y, z) == 2)
        {
            texturePos = tGrassTop;
        }

        Cube(texturePos);
    }

    void CubeNorth(int x, int y, int z, int block)
    {
        //CubeNorth
        /*vertices.Add(new Vector3(x + 1, y - 1, z + 1));
        vertices.Add(new Vector3(x + 1, y, z + 1));
        vertices.Add(new Vector3(x, y, z + 1));
        vertices.Add(new Vector3(x, y - 1, z + 1));*/

        vertices.Add(new Vector3(x + 1, y - 1, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x, y, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x, y - 1, z + 1) * VoxelWorld.World.adjustedScale);

        Vector2 texturePos = stoneUV;

        if (Block(x, y, z) == 1)
        {
            texturePos = stoneUV;
        }
        else if (Block(x, y, z) == 2)
        {
            texturePos = grassUV;
        }

        Cube(texturePos);
    }

    void CubeEast(int x, int y, int z, int block)
    {

        //CubeEast
        /*vertices.Add(new Vector3(x + 1, y - 1, z));
        vertices.Add(new Vector3(x + 1, y, z));
        vertices.Add(new Vector3(x + 1, y, z + 1));
        vertices.Add(new Vector3(x + 1, y - 1, z + 1));*/

        vertices.Add(new Vector3(x + 1, y - 1, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y - 1, z + 1) * VoxelWorld.World.adjustedScale);

        Vector2 texturePos = stoneUV;

        if (Block(x, y, z) == 1)
        {
            texturePos = stoneUV;
        }
        else if (Block(x, y, z) == 2)
        {
            texturePos = grassUV;
        }


        Cube(texturePos);
    }

    void CubeSouth(int x, int y, int z, int block)
    {

        //CubeSouth
        /*vertices.Add(new Vector3(x, y - 1, z));
        vertices.Add(new Vector3(x, y, z));
        vertices.Add(new Vector3(x + 1, y, z));
        vertices.Add(new Vector3(x + 1, y - 1, z));*/

        vertices.Add(new Vector3(x, y - 1, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x, y, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y - 1, z) * VoxelWorld.World.adjustedScale);

        Vector2 texturePos = stoneUV;

        if (Block(x, y, z) == 1)
        {
            texturePos = stoneUV;
        }
        else if (Block(x, y, z) == 2)
        {
            texturePos = grassUV;
        }

        Cube(texturePos);
    }

    void CubeWest(int x, int y, int z, int block)
    {

        //CubeWest
        /*vertices.Add(new Vector3(x, y - 1, z + 1));
        vertices.Add(new Vector3(x, y, z + 1));
        vertices.Add(new Vector3(x, y, z));
        vertices.Add(new Vector3(x, y - 1, z));*/

        vertices.Add(new Vector3(x, y - 1, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x, y, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x, y, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x, y - 1, z) * VoxelWorld.World.adjustedScale);

        Vector2 texturePos = stoneUV;

        if (Block(x, y, z) == 1)
        {
            texturePos = stoneUV;
        }
        else if (Block(x, y, z) == 2)
        {
            texturePos = grassUV;
        }

        Cube(texturePos);
    }

    void CubeBot(int x, int y, int z, int block)
    {

        //CubeBot
        /*vertices.Add(new Vector3(x, y - 1, z));
        vertices.Add(new Vector3(x + 1, y - 1, z));
        vertices.Add(new Vector3(x + 1, y - 1, z + 1));
        vertices.Add(new Vector3(x, y - 1, z + 1));*/

        vertices.Add(new Vector3(x, y - 1, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y - 1, z) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x + 1, y - 1, z + 1) * VoxelWorld.World.adjustedScale);
        vertices.Add(new Vector3(x, y - 1, z + 1) * VoxelWorld.World.adjustedScale);

        Vector2 texturePos = stoneUV;

        if (Block(x, y, z) == 1)
        {
            texturePos = stoneUV;
        }
        else if (Block(x, y, z) == 2)
        {
            texturePos = grassUV;
        }

        Cube(texturePos);
    }

    void Cube(Vector2 texturePos)
    {
        triangles.Add(faceCount * 4); //1
        triangles.Add(faceCount * 4 + 1); //2
        triangles.Add(faceCount * 4 + 2); //3
        triangles.Add(faceCount * 4); //1
        triangles.Add(faceCount * 4 + 2); //3
        triangles.Add(faceCount * 4 + 3); //4

        uvCoords.Add(new Vector2(tUnit * texturePos.x + tUnit, tUnit * texturePos.y));
        uvCoords.Add(new Vector2(tUnit * texturePos.x + tUnit, tUnit * texturePos.y + tUnit));
        uvCoords.Add(new Vector2(tUnit * texturePos.x, tUnit * texturePos.y + tUnit));
        uvCoords.Add(new Vector2(tUnit * texturePos.x, tUnit * texturePos.y));

        faceCount++; // Add this line
    }


    #endregion
}

   