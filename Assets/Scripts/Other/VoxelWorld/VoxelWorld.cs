﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = System.Random;

public class VoxelWorld:MonoBehaviour
{
    public static VoxelWorld World;

    

    public GameObject chunkPrefab;

    private int[,,] data;
    public GameObject[,,] chunks;
    public int chunkSize = 16;
    public float scale = 1;
    public int worldX = 16;
    public int worldY = 16;
    public int worldZ = 16;

    private float tUnit = 0.0625f;
    private Vector2 stoneUV = new Vector2(1, 15);
    private Vector2 grassUV = new Vector2(3, 15);


    public float adjustedScale {get{ return  scale * 0.5f; } }

    private void Awake()
    {
        World = this;
        data = new int[worldX, worldY, worldZ];

    }

    private void Start()
    {
        GenTerrain();
    }

    void GenTerrain()
    {
        data = new int[worldX, worldY, worldZ];

        for (int x = 0; x < worldX; x++)
        {
            for (int z = 0; z < worldZ; z++)
            {
                /*int stone = PerlinNoise(x, 0, z, 300, 4, 1f);
                stone += PerlinNoise(x, 0, z, 400, 5, 0) + 10;*/
                int dirt = PerlinNoise(x, 0, z, 300, 7, 2); //Added +1 to make sure minimum grass height is 1

                /*int stone = PerlinNoise(x, 0, z, 200, 4, 1f);
                stone += PerlinNoise(x, 300, z, 300, 5, 0) + 10;
                int dirt = PerlinNoise(x, 100, z, 200, 4, 3) + 1; //Added +1 to make sure minimum grass height is 1*/

                for (int y = 0; y < worldY; y++)
                {
                    /*if (y <= stone)
                    {
                        data[x, y, z] = 1;
                    }
                    else if (y <= dirt + stone)
                    { //Changed this line thanks to a comment
                        data[x, y, z] = 2;
                    }*/
                    if(y<=dirt)
                    data[x, y, z] = 2;

                }
            }
        }

        chunks = new GameObject[Mathf.FloorToInt(worldX / chunkSize),
            Mathf.FloorToInt(worldY / chunkSize),
            Mathf.FloorToInt(worldZ / chunkSize)];

        for (int x = 0; x < chunks.GetLength(0); x++)
        {
            for (int y = 0; y < chunks.GetLength(1); y++)
            {
                for (int z = 0; z < chunks.GetLength(2); z++)
                {

                    chunks[x, y, z] = Instantiate(chunkPrefab,
                        new Vector3(x * chunkSize * adjustedScale, y * chunkSize * adjustedScale, z * chunkSize * adjustedScale),
                        new Quaternion(0, 0, 0, 0)) as GameObject;

                    ChunkData chunkData = chunks[x, y, z].GetComponent<ChunkData>();
                    
                    chunkData.chunkSize = chunkSize;
                    chunkData.chunkX = x * chunkSize;
                    chunkData.chunkY = y * chunkSize;
                    chunkData.chunkZ = z * chunkSize;
                }
            }
        }

    }

    int PerlinNoise(int x, int y, int z, float scale, float height, float power)
    {
        float rValue;
        rValue = Noise.Noise.GetNoise(((double)x) / scale, ((double)y) / scale, ((double)z) / scale);
        rValue *= height;

        if (power != 0)
        {
            rValue = Mathf.Pow(rValue, power);
        }

        return (int)rValue;
    }

    public int Block(int x, int y, int z)
    {
        if (x >= worldX || x < 0 || y >= worldY || y < 0 || z >= worldZ || z < 0)
        {
            return 1;
        }

        return data[x, y, z];
    }

}

/*

public class MeshFuntions
{
    public static Mesh ReduceMesh()
    {
        List<Vector3> vertices = new List<Vector3>();
        List<int> elements = new List<int>();
        List<Vector3> uvs = new List<Vector3>();
        List<Vector3> colours = new List<Vector3>();

        ///List<int> noCollision = new List<int> { 0, 200, 201, 202, 203, 254 };
        //Chunks Size
        int size = 10;

        //Sweep over 3-axes
        for (int d = 0; d < 3; d++)
        {

            int i, j, k, l, w, h, u = (d + 1) % 3, v = (d + 2) % 3;

            int[] x = new int[3];
            int[] q = new int[3];
            int[] mask = new int[(size + 1) * (size + 1)];

            q[d] = 1;

            for (x[d] = -1; x[d] < size;)
            {

                // Compute the mask
                int n = 0;
                for (x[v] = 0; x[v] < size; ++x[v])
                {
                    for (x[u] = 0; x[u] < size; ++x[u], ++n)
                    {


                        int a = 0;
                        if (0 <= x[d])
                        {
                            a = (int) World.GetBlock(chunk, x[0], x[1], x[2]).Type;
                            /*if (noCollision.IndexOf(a) != -1)
                            {
                                a = 0;
                            }#1#
                        }

                        int b = 0;
                        if (x[d] < size - 1)
                        {
                            b = (int) World.GetBlock(chunk, x[0] + q[0], x[1] + q[1], x[2] + q[2]).Type;
                            /*if (noCollision.IndexOf(b) != -1)
                            {
                                b = 0;
                            }#1#
                        }

                        if (a != -1 && b != -1 && a == b)
                        {
                            mask[n] = 0;
                        }
                        else if (a > 0)
                        {
                            a = 1;
                            mask[n] = a;
                        }

                        else
                        {
                            b = 1;
                            mask[n] = -b;
                        }

                    }


                }

                // Increment x[d]
                ++x[d];

                // Generate mesh for mask using lexicographic ordering
                n = 0;
                for (j = 0; j < size; ++j)
                {
                    for (i = 0; i < size;)
                    {
                        var c = mask[n];
                        if (c > -3)
                        {
                            // Compute width
                            for (w = 1; c == mask[n + w] && i + w < size; ++w)
                            {
                            }

                            // Compute height
                            bool done = false;
                            for (h = 1; j + h < size; ++h)
                            {
                                for (k = 0; k < w; ++k)
                                {
                                    if (c != mask[n + k + h * size])
                                    {
                                        done = true;
                                        break;
                                    }
                                }

                                if (done) break;
                            }

                            bool flip = false;
                            x[u] = i;
                            x[v] = j;
                            int[] du = new int[3];
                            int[] dv = new int[3];
                            if (c > -1)
                            {
                                du[u] = w;
                                dv[v] = h;
                            }
                            else
                            {
                                flip = true;
                                c = -c;
                                du[u] = w;
                                dv[v] = h;
                            }


                            Vector3 v1 = new Vector3(x[0], x[1], x[2]);
                            Vector3 v2 = new Vector3(x[0] + du[0], x[1] + du[1], x[2] + du[2]);
                            Vector3 v3 = new Vector3(x[0] + du[0] + dv[0], x[1] + du[1] + dv[1], x[2] + du[2] + dv[2]);
                            Vector3 v4 = new Vector3(x[0] + dv[0], x[1] + dv[1], x[2] + dv[2]);

                            if (c > 0 && !flip)
                            {
                                AddFace(v1, v2, v3, v4, vertices, elements, 0);
                            }

                            if (flip)
                            {
                                AddFace(v4, v3, v2, v1, vertices, elements, 0);
                            }

                            // Zero-out mask
                            for (l = 0; l < h; ++l)
                            for (k = 0; k < w; ++k)
                            {
                                mask[n + k + l * size] = 0;
                            }

                            // Increment counters and continue
                            i += w;
                            n += w;
                        }

                        else
                        {
                            ++i;
                            ++n;
                        }
                    }
                }
            }
        }

        Mesh mesh = new Mesh();
        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = elements.ToArray();
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();


        return mesh;

    }

    private static void AddFace(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, List<Vector3> vertices, List<int> elements, int order)
    {
        if (order == 0)
        {
            int index = vertices.Count;

            vertices.Add(v1);
            vertices.Add(v2);
            vertices.Add(v3);
            vertices.Add(v4);

            elements.Add(index);
            elements.Add(index + 1);
            elements.Add(index + 2);
            elements.Add(index + 2);
            elements.Add(index + 3);
            elements.Add(index);

        }

        if (order == 1)
        {
            int index = vertices.Count;

            vertices.Add(v1);
            vertices.Add(v2);
            vertices.Add(v3);
            vertices.Add(v4);

            elements.Add(index);
            elements.Add(index + 3);
            elements.Add(index + 2);
            elements.Add(index + 2);
            elements.Add(index + 1);
            elements.Add(index);

        }
    }
}
*/

/*    public enum ChunkState : int
{
    Invalid = 0,
    Base = 1,
    Terra = 10,
    ReGen = 11,
    Light = 15,
    Post_Light = 16,
    Mesh = 20,
    Render = 30

}*/


/*public class Chunk
{

    //render mesh data
    public List<int> triangles;
    public List<int>[] subtriangles;
    public List<Vector2> uvs;
    public List<Vector3> vertices;
    public List<Color> colours;
    public List<Vector3> normals;
    public List<Vector4> tangents;
    public List<Lux> lightsources;
    public List<IntVect> sunlitBlocks;

    //collision mesh data
    public List<int> col_triangles;
    public List<Vector3> col_vertices;



    //chunk world position
    public int x;
    public int y;
    public int z;

    public GameObject chunkObject;
    public ChunkState state;



    public bool AboveReady(ChunkState s)
    {

        if (y == World.yChunkRadius - 1) { return true; }

        int ts = (int)s;
        int cs = (int)World.GetChunkState(x, y + 1, z);

        if (cs == 0) { return true; }
        if (cs >= ts) { return true; }
        return false;
    }

    public bool SurroundingState(ChunkState cstate)
    {
        ChunkState north = World.GetChunkState(x, y, z + 1);
        if ((int)north < (int)cstate && north != ChunkState.Invalid) { return false; }

        ChunkState south = World.GetChunkState(x, y, z - 1);
        if ((int)south < (int)cstate && south != ChunkState.Invalid) { return false; }

        ChunkState east = World.GetChunkState(x + 1, y, z);
        if ((int)east < (int)cstate && east != ChunkState.Invalid) { return false; }

        ChunkState west = World.GetChunkState(x - 1, y, z);
        if ((int)west < (int)cstate && west != ChunkState.Invalid) { return false; }
        if (!AboveReady(cstate)) { return false; }

        /*
        ChunkState stack;
        for (int i = 0; i < World.yChunkRadius; i++)
        {
            stack = World.GetChunkState(x, i, z);
            if ((int)stack < (int)cstate && stack != ChunkState.Invalid) { return false; }
        }
        #1#


        return true;
    }

    public void Init(int _x, int _y, int _z)
    {
        x = _x; y = _y; z = _z;
        state = ChunkState.Base;

    }
    public void ReBuild()
    {
        sunlitBlocks = new List<IntVect>();
        col_vertices = new List<Vector3>();
        col_triangles = new List<int>();
        vertices = new List<Vector3>();
        triangles = new List<int>();
        uvs = new List<Vector2>();
        colours = new List<Color>();
        normals = new List<Vector3>();
        tangents = new List<Vector4>();

        subtriangles = new List<int>[Mats.matCount];
        for (int n = 0; n < Mats.matCount; n++) { subtriangles[n] = new List<int>(); }

    }





}*/
