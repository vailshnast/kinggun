﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TestVoxelGeneration : MonoBehaviour
{
    public GameObject voxel;
    public int width = 500;
    public int height = 500;
    public int depth = 500;

    public int smoothRatio = 5;

    public int randomFillPercent = 43;

    private int[,,] voxelData;

    void Start()
    {
        GenerateMap();
    }

    void GenerateMap()
    {
        voxelData = new int[width, height, depth];
        
        RandomFillMap();

        for (int i = 0; i < smoothRatio; i++)
        {
            SmoothMap();
        }

        //        if(removeRegions)
        //            ProcessMap();

        GenerateTiles();
    }

    private void GenerateTiles()
    {
        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < depth; z++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (voxelData[x, y, z] == 1)
                        Instantiate(voxel, new Vector3(x * voxel.transform.localScale.x, y * voxel.transform.localScale.y, z * voxel.transform.localScale.z), Quaternion.identity, transform);
                }
            }
        }
    }

    void RandomFillMap()
    {
        string seed = DateTime.Now.Ticks.ToString();

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < depth; z++)
            {
                for (int y = 0; y < height; y++)
                {
                    voxelData[x, y, z] = pseudoRandom.Next(0, 100) < randomFillPercent ? 1 : 0;
                }
            }
        }
    }

    void SmoothMap()
    {
        /*for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                    GridNodes[x, y].nodeType = NodeType.Box;
                else if (neighbourWallTiles < 4)
                    GridNodes[x, y].nodeType = NodeType.Floor;

            }
        }*/
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        /*int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (IsInMapRange(neighbourX, neighbourY))
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        if (GridNodes[neighbourX, neighbourY].nodeType == NodeType.Box)
                            wallCount++;
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }*/
        return 0;
    }
}
