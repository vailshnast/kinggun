﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Random = System.Random;


public class MapGeneration : MonoBehaviour
{
    public int width;
    public int height;

    public string seed;
    public bool useRandomSeed;
    public bool removeRegions;
    [Range(0, 100)]
    public int randomFillPercent;

    private Node[,] GridNodes;
    public GameObject boxTile;
    public GameObject floorTile;
    public GameObject cornerTile;
    public GameObject wallTile;

    public GameObject TwoWayRoadTile;
    public GameObject CornerRoadTile;
    public GameObject CrossRoadTile;
    public GameObject ThreeWayRoadTile;
    public GameObject NewRoadPrefab;
    public int smoothRatio;
    List<Node> path = new List<Node>();



    private Vector3 testPoint = Vector3.zero;
    void Start()
    {
        GenerateMap();
    }

    #region Map Generation

    void GenerateMap()
    {
        GridNodes = new Node[width, height];

        RandomFillMap();
        

        for (int i = 0; i < smoothRatio; i++)
        {
            SmoothMap();
        }

//        if(removeRegions)
//            ProcessMap();

        InsertTiles();
        GenerateTiles();       
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //TestPath();
            testPoint = GetRadomPointOnMap();
        }
    }

    [ContextMenu("TestPath")]
    public void TestPath()
    {

        List<Node> nodes = GridNodes.Cast<Node>().ToList();
        List<Node> walkable = nodes.Where(t => t.walkable).ToList();

        Node startNode = walkable[UnityEngine.Random.Range(0, walkable.Count)];
        Node endNode = walkable[UnityEngine.Random.Range(0, walkable.Count)];
        
        path = FindPath(startNode, endNode);

        #region MyRegion
        //        for (int x = 0; x < width; x++)
        //        {
        //            for (int y = 0; y < height; y++)
        //            {
        //                if (start == GridNodes[x, y].worldObject)
        //                    startNode = GridNodes[x, y];
        //
        //                if (end == GridNodes[x, y].worldObject)
        //                    endNode = GridNodes[x, y];
        //            }
        //        }
        /*for (int i = 0; i < path.Count; i++)
        {
            GameObject roadTile = null;
            if (i == 0 || i == path.Count - 1)
            {
                //                GameObject roadTile = Instantiate(TwoWayRoadTile, path[i].worldObject.transform.position, Quaternion.identity, transform);
                //
                //                Vector3 direction = 
                //
                //                Destroy(path[i].worldObject);
                roadTile = Instantiate(TwoWayRoadTile, path[i].worldObject.transform.position, Quaternion.identity, transform);

                if (i == 0)
                {
                    if(path[i].worldObject.transform.position.x != path[i + 1].worldObject.transform.position.x)
                        roadTile.transform.rotation = Quaternion.Euler(0, 90, 0);
                    else
                        roadTile.transform.rotation = Quaternion.Euler(0, 0, 0);
                }

                else
                {
                    if (path[i].worldObject.transform.position.x != path[i - 1].worldObject.transform.position.x)
                        roadTile.transform.rotation = Quaternion.Euler(0, 90, 0);
                    else
                        roadTile.transform.rotation = Quaternion.Euler(0, 0, 0);
                }


                Destroy(path[i].worldObject);
                path[i].worldObject = roadTile;
            }
            else
            {
                bool x = path[i + 1].worldObject.transform.position.x == path[i - 1].worldObject.transform.position.x;
                bool z = path[i + 1].worldObject.transform.position.z == path[i - 1].worldObject.transform.position.z;

                //Debug.Log(path[i + 1].worldObject.transform.position + " / " + path[i - 1].worldObject.transform.position + " / " + path[i].worldObject.transform.position + " / " + (x || z));

                if (x || z)
                {

                    roadTile = Instantiate(TwoWayRoadTile, path[i].worldObject.transform.position, Quaternion.identity, transform);

                    if (path[i].worldObject.transform.position.x != path[i + 1].worldObject.transform.position.x)
                        roadTile.transform.rotation = Quaternion.Euler(0, 90, 0);
                    else
                        roadTile.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                else
                {
                    List<Node> neighbours = new List<Node> {path[i + 1], path[i - 1]};
                    Quaternion rotation = Quaternion.identity;
                    bool left = false;
                    bool right = false;
                    bool top = false;
                    bool bottom = false;

                    for (int index = 0; index < neighbours.Count; index++)
                    {
                        if (neighbours[index].x > path[i].x)
                            right = true;
                        if (neighbours[index].x < path[i].x)
                            left = true;
                        if (neighbours[index].y > path[i].y)
                            top = true;
                        if (neighbours[index].y < path[i].y)
                            bottom = true;
                    }


                    if (right && top)
                    {
                        rotation = Quaternion.Euler(0, 90, 0);
                    }

                    if (right && bottom)
                    {
                        rotation = Quaternion.Euler(0, 180, 0);
                    }
                    if (left && bottom)
                    {
                        rotation = Quaternion.Euler(0, 270, 0);
                    }
                    if (left && top)
                    {
                        rotation = Quaternion.Euler(0, 0, 0);
                    }


                    roadTile = Instantiate(CornerRoadTile, path[i].worldObject.transform.position, rotation, transform);
                }


                Destroy(path[i].worldObject);
                path[i].worldObject = roadTile;
            }
        }*/

        #endregion

        Vector3 previousDirection = new Vector2(path[1].x,path[1].y) - new Vector2(path[0].x, path[0].y);
        List<Vector3> proceduralRoadNodes = new List<Vector3>();

        proceduralRoadNodes.Add(path[0].worldObject.transform.position);

        for (int i = 1; i < path.Count - 1; i++)
        {
            Vector3 currrentDirection = new Vector2(path[i + 1].x, path[i + 1].y) - new Vector2(path[i].x, path[i].y);

            if (currrentDirection != previousDirection)
            {
                previousDirection = currrentDirection;
                proceduralRoadNodes.Add(path[i].worldObject.transform.position);
            }              
        }

        proceduralRoadNodes.Add(path[path.Count - 1].worldObject.transform.position);
        GameObject road = Instantiate(NewRoadPrefab, Vector3.zero + new Vector3(0,0.5f,0), Quaternion.identity);
        CurveImplementation.instance.GenerateRoad(proceduralRoadNodes, road.GetComponent<MeshFilter>());

    }
    
    private void InsertTiles()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                //If tile is a box
                if (GridNodes[x, y].nodeType == NodeType.Box)
                {
                    List<Node> emptyNeighbours = GetNeighbours(NodeType.Floor, x, y);

                    for (int i = 0; i < emptyNeighbours.Count; i++)
                    {
                        List<Node> boxNeighbours = GetNeighbours(NodeType.Box, emptyNeighbours[i].x, emptyNeighbours[i].y);

                        if (boxNeighbours.Count == 1)
                        {
                            GridNodes[emptyNeighbours[i].x, emptyNeighbours[i].y].nodeType = NodeType.Wall;

                            List<Node> floorNeighbours =  GetNeighbours(NodeType.Floor, emptyNeighbours[i].x, emptyNeighbours[i].y);
                            for (int j = 0; j < floorNeighbours.Count; j++)
                            {
                                floorNeighbours[j].movePenalty = 5;
                                List<Node> floorNeighbours2 = GetNeighbours(NodeType.Floor, floorNeighbours[j].x, floorNeighbours[j].y);
                                for (int k = 0; k < floorNeighbours2.Count; k++)
                                {
                                    floorNeighbours2[k].movePenalty = 5;
                                }
                            }

                        }

                        if (boxNeighbours.Count == 2)
                        {
                            GridNodes[emptyNeighbours[i].x, emptyNeighbours[i].y].nodeType = NodeType.Corner;

                            List<Node> floorNeighbours = GetNeighbours(NodeType.Floor, emptyNeighbours[i].x, emptyNeighbours[i].y);
                            for (int j = 0; j < floorNeighbours.Count; j++)
                            {
                                floorNeighbours[j].movePenalty = 5;
                                List<Node> floorNeighbours2 = GetNeighbours(NodeType.Floor, floorNeighbours[j].x, floorNeighbours[j].y);
                                for (int k = 0; k < floorNeighbours2.Count; k++)
                                {
                                    floorNeighbours2[k].movePenalty = 5;
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    public List<Node> GetNeighbours(NodeType tileType , int x , int y)
    {
        List<Node> neighbours = new List<Node>();

        if (x + 1 < width && GridNodes[x + 1, y].nodeType == tileType)
            neighbours.Add(GridNodes[x + 1, y]);

        if (x - 1 > 0 && GridNodes[x - 1, y].nodeType == tileType)
            neighbours.Add(GridNodes[x - 1, y]);

        if (y + 1 < width && GridNodes[x, y + 1].nodeType == tileType)
            neighbours.Add(GridNodes[x, y + 1]);

        if (y - 1 > 0 && GridNodes[x, y - 1].nodeType == tileType)
            neighbours.Add(GridNodes[x, y - 1]);

        return neighbours;
    }
    public List<Node> GetNeighbours(int x, int y)
    {
        List<Node> neighbours = new List<Node>();

        if (x + 1 < width)
            neighbours.Add(GridNodes[x + 1, y]);

        if (x - 1 > 0)
            neighbours.Add(GridNodes[x - 1, y]);

        if (y + 1 < width)
            neighbours.Add(GridNodes[x, y + 1]);

        if (y - 1 > 0)
            neighbours.Add(GridNodes[x, y - 1]);

        return neighbours;
    }

    public List<Node> GetNeighboursForPathFind(int x, int y)
    {
        List<Node> neighbours = new List<Node>();

        if (x + 1 < width)
            neighbours.Add(GridNodes[x + 1, y]);

        if (x - 1 > 0)
            neighbours.Add(GridNodes[x - 1, y]);

        if (y + 1 < width)
            neighbours.Add(GridNodes[x, y + 1]);

        if (y - 1 > 0)
            neighbours.Add(GridNodes[x, y - 1]);

        //Diagonal

        if (x - 1 > 0 && y + 1 < width)
            neighbours.Add(GridNodes[x - 1, y + 1]);

        if (x + 1 < width && y + 1 < width)
            neighbours.Add(GridNodes[x + 1, y + 1]);

        if (x + 1 < width && y - 1 > 0)
            neighbours.Add(GridNodes[x + 1, y -1]);

        if (x - 1 > 0 && y - 1 > 0)
            neighbours.Add(GridNodes[x - 1, y - 1]);

        return neighbours;
    }

    private void GenerateTiles()
    { 
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                
                switch (GridNodes[x, y].nodeType)
                {
                     

                    case NodeType.Floor:
                        GridNodes[x, y].worldObject = Instantiate(floorTile, new Vector3(x * 5.04f, 0, y * 5.04f), Quaternion.identity, transform);
                        GridNodes[x, y].walkable = true;
                        break;
                    case NodeType.Box:
                        GridNodes[x, y].worldObject = Instantiate(boxTile, new Vector3(x * 5.04f, 0, y * 5.04f), Quaternion.identity, transform);
                        break;
                    case NodeType.Wall:

                        List<Node> neighbours = GetNeighbours(NodeType.Box, x, y);
                        Quaternion rotation;

                        
                        rotation = x == neighbours[0].x ? Quaternion.Euler(0, neighbours[0].y > y ? 90 : 270, 0) : Quaternion.Euler(0, neighbours[0].x > x ? 180 : 0, 0);

                        GridNodes[x, y].worldObject = Instantiate(wallTile, new Vector3(x * 5.04f, 0, y * 5.04f), rotation, transform);
                        GridNodes[x, y].walkable = false;
                        break;
                    case NodeType.Corner:

                        neighbours = GetNeighbours(NodeType.Box, x, y);
                        rotation = Quaternion.identity;

                        bool left = false;
                        bool right = false;
                        bool top = false;
                        bool bottom = false;

                        for (int i = 0; i < neighbours.Count; i++)
                        {
                            if (neighbours[i].x > x)
                                right = true;
                            if (neighbours[i].x < x)
                                left = true;
                            if (neighbours[i].y > y)
                                top = true;
                            if (neighbours[i].y < y)
                                bottom = true;
                        }


                        if (right && top)
                        {
                            rotation = Quaternion.Euler(0, 90, 0);
                        }

                        if (right && bottom)
                        {
                            rotation = Quaternion.Euler(0, 180, 0);
                        }
                        if (left && bottom)
                        {
                            rotation = Quaternion.Euler(0, 270, 0);
                        }
                        if (left && top)
                        {
                            rotation = Quaternion.Euler(0, 0, 0);
                        }
                        GridNodes[x, y].walkable = false;
                        GridNodes[x, y].worldObject = Instantiate(cornerTile, new Vector3(x * 5.04f, 0, y * 5.04f), rotation, transform);
                        break;
                }
            }
        }
    }

    /*#region Regions

    public void ProcessMap()
    {
        List<List<Node>> wallRegions = GetRegions(1);
        int wallThresholdSize = 50;

        foreach (List<Node> wallRegion in wallRegions)
        {
            if (wallRegion.Count < wallThresholdSize)
            {
                foreach (Node tile in wallRegion)
                {
                    map[tile.tileX, tile.tileY] = 0;
                }
            }
        }

        List<List<Node>> roomRegions = GetRegions(0);
        int roomThresholdSize = 50;

        foreach (List<Node> roomRegion in roomRegions)
        {
            if (roomRegion.Count < roomThresholdSize)
            {
                foreach (Node tile in roomRegion)
                {
                    map[tile.tileX, tile.tileY] = 1;
                }
            }
        }
    }

    List<List<Node>> GetRegions(int tileType)
    {
        List<List<Node>> regions = new List<List<Node>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                {
                    List<Node> newRegion = GetRegionTiles(x, y);
                    regions.Add(newRegion);

                    foreach (Node tile in newRegion)
                    {
                        mapFlags[tile.tileX, tile.tileY] = 1;
                    }
                }
            }
        }

        return regions;
    }

    List<Node> GetRegionTiles(int startX, int startY)
    {
        List<Node> tiles = new List<Node>();
        int[,] mapFlags = new int[width, height];
        int tileType = map[startX, startY];

        Queue<Node> queue = new Queue<Node>();
        queue.Enqueue(new Node(startX, startY));
        mapFlags[startX, startY] = 1;

        while (queue.Count > 0)
        {
            Node tile = queue.Dequeue();
            tiles.Add(tile);

            for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
            {
                for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                {
                    if (IsInMapRange(x, y) && (y == tile.tileY || x == tile.tileX))
                    {
                        if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                        {
                            mapFlags[x, y] = 1;
                            queue.Enqueue(new Node(x, y));
                        }
                    }
                }
            }
        }

        return tiles;
    }

    #endregion
*/
    bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = DateTime.Now.Ticks.ToString();
        }

        Random pseudoRandom = new Random(seed.GetHashCode());
        
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GridNodes[x, y] = new Node(x, y);
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    GridNodes[x, y].nodeType = NodeType.Box;
                }
                else
                {
                    GridNodes[x, y].nodeType = pseudoRandom.Next(0, 100) < randomFillPercent ? NodeType.Box : NodeType.Floor;
                }
            }
        }
    }

    void SmoothMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neighbourWallTiles = GetSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                    GridNodes[x, y].nodeType = NodeType.Box;
                else if (neighbourWallTiles < 4)
                    GridNodes[x, y].nodeType = NodeType.Floor;

            }
        }
    }
    
    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++)
        {
            for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++)
            {
                if (IsInMapRange(neighbourX, neighbourY))
                {
                    if (neighbourX != gridX || neighbourY != gridY)
                    {
                        if (GridNodes[neighbourX, neighbourY].nodeType == NodeType.Box)
                            wallCount++;
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

    private void RandomPoints()
    {
        int pointsCount = 4;
        int minRadius = 50;
        int maxRadius = 100;
        int subRadius = 50;       
        
        //Random first Point
    }

    private Vector3 GetRadomPointOnMap()
    {
        float maxX = GridNodes[GridNodes.GetLength(0) - 1, GridNodes.GetLength(1) - 1].worldObject.transform.position.x;
        float maxZ = GridNodes[GridNodes.GetLength(0) - 1, GridNodes.GetLength(1) - 1].worldObject.transform.position.z;

        return new Vector3(UnityEngine.Random.Range(85, maxX - 85), 0, UnityEngine.Random.Range(85, maxZ - 85));
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawSphere(testPoint, 170);
    }

    #endregion
                            
    #region Path
    
    public List<Node> FindPath (Node origin, Node destination)
    {
        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();

        openSet.Add(origin);

        while (openSet.Count > 0)
        {
            Node currentNode = openSet[0];

            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < currentNode.fCost || openSet[i].fCost == currentNode.fCost)                   
                    if(openSet[i].hCost < currentNode.hCost) 
                    currentNode = openSet[i];

            }

            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            if (currentNode == destination)
            {
                return RetracePath(origin, destination);
            }
            
            List<Node> neighbours = GetNeighboursForPathFind(currentNode.x, currentNode.y);
            for (int i = 0; i < neighbours.Count; i++)
            {
                if (!GridNodes[neighbours[i].x, neighbours[i].y].walkable || closedSet.Contains(neighbours[i]))
                    continue;

                int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbours[i]) /*+ UnityEngine.Random.Range(0, 6)*/ + neighbours[i].movePenalty; 

                if (newMovementCostToNeighbour < neighbours[i].gCost || !openSet.Contains(neighbours[i]))
                {
                    neighbours[i].gCost = newMovementCostToNeighbour;
                    neighbours[i].hCost = GetDistance(neighbours[i], destination);
                    neighbours[i].parent = currentNode;

                    if (!openSet.Contains(neighbours[i]))
                        openSet.Add(neighbours[i]);
                }
            }          
        }

        return null;
    }
    
    List<Node> RetracePath(Node startNode, Node destination)
    {
        List<Node> path = new List<Node>();
        Node currentNode = destination;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();
        return path;
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = Mathf.Abs(nodeA.x - nodeB.x);
        int dstY = Mathf.Abs(nodeA.y - nodeB.y);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }

    #endregion
}
public enum TileType
{
    Box,
    Floor,
}
[System.Serializable]
public class Node
{
    public bool walkable;
       
    public int x;
    public int y;
    
    public int gCost;
    public int hCost;
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }

    public Node parent;
    
    public NodeType nodeType;

    public GameObject worldObject;

    public int movePenalty;
    
    public Node(int x, int y)
    {
        this.x = x;
        this.y = y;

        movePenalty = 0;

        gCost = 0;
        hCost = 0;
    }
}
public enum NodeType
{
    Box,
    Corner,
    Wall,
    Floor
}