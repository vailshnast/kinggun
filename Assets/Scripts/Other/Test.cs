﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Test : MonoBehaviour
{
    public static Test instance;
    public float speed;
    public float timeInFuture = 0.05f;
    public float timeStep = 0.005f;
    public GameObject sphere;
    public float rate = 0.7f;
    private float collisioDist;
    public List<Transform> spheres;
    public Transform test;
    private List<DirectionDecision> directionCosts;
    private bool evading;
    void Awake()
    {
        spheres = new List<Transform>();

        directionCosts = new List<DirectionDecision>()
        {
            //new DirectionDecision(Vector3.up),
            //new DirectionDecision(-Vector3.up),
            new DirectionDecision(-Vector3.right),
            new DirectionDecision(Vector3.right),
           
            /*new DirectionDecision((Vector3.right + Vector3.up).normalized),
            new DirectionDecision((-Vector3.right + Vector3.up).normalized),
            new DirectionDecision((Vector3.right - Vector3.up).normalized),
            new DirectionDecision((-Vector3.right - Vector3.up).normalized)*/
        };

        collisioDist = 0.65f;
        //collisioDist = 1;
        // Vector3 futurePosition = currentPosition + (facingDir * (speed * time));
        instance = this;
        StartCoroutine(SpawnShit());
    }


    // Update is called once per frame
    void Update()
    {
        Evade();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.position += Vector3.right * speed * 0.05f;
        }
    }


    public float GetDist( Vector3 end)
    {
        return Vector3.Distance(transform.position,end);
    }
    public float GetDist(Vector3 start, Vector3 end)
    {
        return Vector3.Distance(start, end);
    }
    private void Evade()
    {
        if(!evading)
        MakeStepDecision();
    }

    private void MakeStepDecision()
    {
        evading = true;
        Vector3 futurePos;
        for (int i = 0; i < directionCosts.Count; i++)
        {
            directionCosts[i].cost = 0;
        }
        for (int i = 0; i < directionCosts.Count; i++)
        {

            float time = timeStep;
            futurePos = transform.position + directionCosts[i].direction * speed * time;
            while (time < timeInFuture)
            {
                for (int j = 0; j < spheres.Count; j++)
                {
                    if (GetDist(futurePos, GetSphereFuturePos(spheres[j], time)) < collisioDist)
                        directionCosts[i].cost += 1;
                }

                time += timeStep;
            }


           

        }


        var selected = directionCosts[Random.Range(0, directionCosts.Count)];
        for (int i = 0; i < directionCosts.Count; i++)
        {

            if (selected.cost < directionCosts[i].cost)
                selected = directionCosts[i];
        }
        Vector3 pos = transform.position + selected.direction * speed * Time.deltaTime;
        transform.position = pos;
        evading = false;

    }

    private Vector3 GetSphereFuturePos(Transform sphere, float timeStep)
    {
        return sphere.position + Vector3.down * 5 * timeStep;
    }

    private IEnumerator SpawnShit()
    {
        Instantiate(sphere, new Vector3(/*transform.position.x*/Random.Range(transform.position.x + 7,transform.position.x - 7), transform.position.y + 5, 0), Quaternion.identity);
        yield return new WaitForSeconds(rate);
        StartCoroutine(SpawnShit());
    }

    void OnCollisionEnter(Collision collision)
    {
       Destroy(collision.gameObject);
    }


}

public class DirectionDecision
{
    public Vector3 direction;
    public int cost;

    public DirectionDecision(Vector3 direction)
    {
        this.direction = direction;
        cost = 0;
    }
}