﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereMove : MonoBehaviour
{

    public float speed;
    public float liveTime;

    void Start()
    {
        Destroy(gameObject,liveTime);
        Test.instance.spheres.Add(transform);
    }

    void Update()
    {
        transform.position += Vector3.down * speed * Time.deltaTime;
    }

    void OnDestroy()
    {
        Test.instance.spheres.Remove(transform);
    }
}
