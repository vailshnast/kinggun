﻿
using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(ArenaCreator))]
public class ArenaHelper : Editor
{
    void OnSceneGUI()
    {
        Handles.color = Color.yellow;
        ArenaCreator myObj = (ArenaCreator)target;

        float cornerOffset = 2.286f;
        float tileOffset = 1.524f;


        Vector3[] corners = new Vector3[4];

        corners[0] = new Vector3(myObj.transform.position.x + tileOffset , myObj.transform.position.y, myObj.transform.position.z - tileOffset);

        corners[1] = new Vector3(myObj.transform.position.x - cornerOffset * 2 - tileOffset * myObj.arenaSizeX, myObj.transform.position.y, myObj.transform.position.z - tileOffset);

        corners[2] = new Vector3(myObj.transform.position.x - cornerOffset * 2 - tileOffset * myObj.arenaSizeX, myObj.transform.position.y, myObj.transform.position.z + cornerOffset * 2 + tileOffset * myObj.arenaSizeY);

        corners[3] = new Vector3(myObj.transform.position.x + tileOffset, myObj.transform.position.y ,myObj.transform.position.z + +cornerOffset * 2 + tileOffset * myObj.arenaSizeY);

        if(myObj.showAreaInScene)
        Handles.DrawSolidRectangleWithOutline(corners ,new Color(95 / 255f, 1f, 0, 0.05f), Color.black);
    }
}


