﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager audioManager;

    //public List<AudioClip> clips;

    private AudioSource audioSource;

    void Awake()
    {
        audioManager = this;
        audioSource = GetComponent<AudioSource>();
    }


    public void ChangeMusicVolume(float volume)
    {
        audioSource.volume = volume;
    }

}
